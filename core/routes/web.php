<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'enteweb.base'], function () {

	# Welcome route
	Route::get('/', function () {
	    return view('welcome');
	});

    # Auth Route
    Auth::routes();
});

Route::group(['middleware' => ['auth', 'enteweb.base']], function () {

	# Default home route
	Route::get('/home', 'HomeController@index');

});
