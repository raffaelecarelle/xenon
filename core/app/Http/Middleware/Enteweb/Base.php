<?php

namespace App\Http\Middleware\Enteweb;

use Closure;
use Auth;
use Enteweb;
use App;

use Enteweb\Plugins\Language\Models\Language;
class Base
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Enteweb::checkInstalled()) {
            # Check if the user is activated
            if(Auth::check()) {

                $user = Enteweb::loggedInuser();

                if(!$user->is_active) {
                    if(Enteweb::currentURL() != url('/logout')) {
                        Auth::logout();
                        return redirect()->route('login')->withErrors(trans('enteweb.account_not_active'));
                    }

                }

                # Set App Locale
                if($user->locale) {
                    App::setLocale($user->locale);
                }

            } else {
                if ($request->session()->has('locale')) {
                	App::setLocale(session('locale'));
                }
            }
        }

        return $next($request);
    }
}
