<?php

namespace App\Http\Middleware\Enteweb;

use Closure;
use Auth;
use Enteweb;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Enteweb::checkInstalled()) {
            if(Auth::check()) {
                Enteweb::mustBeAdmin(Enteweb::loggedInUser());
            } else {
                return redirect('/')->with('error', 'You are not logged in');
            }
        }else{
          return redirect()->route('enteweb.install', 'it');
        }
        return $next($request);
    }
}
