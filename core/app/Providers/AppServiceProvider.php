<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
      /*
      *  Forzo la lunghezza massima della string a 191 caratteri perchè con DB
      *  di tipo MyISAM può dare problemi.
      */
      Schema::defaultStringLength(191);

      /*
      * In questo modo é possibile gestire qualsiasi tema attivo in automatico, mediante gli alias in basso.
      */
      $this->app['view']->addNamespace('admin_theme', public_path('themes/' . env('ADM_THEME', config('enteweb.admin_theme') . '/src')));

      $this->app['view']->addNamespace('pub_theme', public_path('themes/' . env('PUB_THEME', config('enteweb.pub_theme') . '/src')));

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
