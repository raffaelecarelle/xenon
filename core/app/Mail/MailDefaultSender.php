<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Enteweb\Plugins\Mail\Models\Mail;

class MailDefaultSender extends Mailable
{
  use Queueable, SerializesModels;

  public $mail;
  public $attachs;

  /**
  * Create a new message instance.
  *
  * @return void
  */
  public function __construct(Mail $mail, $attachments)
  {
    $this->mail = $mail;
    $this->attachs = $attachments;
  }

  /**
  * Build the message.
  *
  * @return $this
  */
  public function build()
  {

    $email = $this->from($this->mail->from)
                  ->subject($this->mail->subject)
                  ->view('mail::Templates.default');

    foreach ($this->attachs as $attachment) {
      $email->attach($attachment);
    }

     return $email;
  }
}
