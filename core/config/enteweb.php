<?php

/*
* File principale di configurazione del CMS Enteweb
*/


return [

  /*
  * Tema amministrazione attivo
  */
  'admin_theme' => env('ADMIN_THEME','Backoffice'),

  /*
  * Tema frontend attivo
  */
  'pub_theme' => env('PUB_THEME', ''),

];
