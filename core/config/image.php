<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    'IMG_MINI_W' => 100,
    'IMG_MINI_H' => 76,

    'IMG_PREVIEW_W' => 196,
    'IMG_PREVIEW_H' => 147,

    'THUMB_W' => 200,
    'THUMB_H' => 200,

    'IMG_SMALL_W' => 550,
    'IMG_SMALL_H' => 400,

    'IMG_MEDIUM_W' => 720,
    'IMG_MEDIUM_H' => 520,

    'IMG_LARGE_W' => 720,
    'IMG_LARGE_H' => 540,

);
