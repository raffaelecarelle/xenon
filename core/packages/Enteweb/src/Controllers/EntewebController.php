<?php

namespace Enteweb\Controllers;

/*
 * Laravel classes
 */
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/*
 * Models
 */
use Enteweb\Plugins\Post\Models\Post;


class EntewebController extends Controller {

  public function index() {

    return redirect()->route('enteweb.dashboard');

  }

  public function dashboard() {

    $pagesCount = Post::type('page')->count();

    $newsCount = Post::type('news')->count();

    $photosCount = Post::type('photo')->where('parent_id', '!=', NULL)->count();

    return view('enteweb::Dashboard.index', compact('pagesCount','newsCount','photosCount'));

  }
}
