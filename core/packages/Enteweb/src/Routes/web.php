<?php

/*
+---------------------------------------------------------------------------+
| Enteweb Routes
+---------------------------------------------------------------------------+
| Pannello amministrativo
+---------------------------------------------------------------------------+
*/

Route::group(['middleware' => ['web','enteweb.base', 'enteweb.auth'], 'prefix' => 'backoffice',
'namespace' => 'Enteweb', 'as' => 'enteweb.'], function () {

  Route::group(['namespace' => 'Controllers'], function () {

    Route::get('/', 'EntewebController@index')->name('index');

    Route::get('/dashboard', 'EntewebController@dashboard')->name('dashboard');
  });

  Route::group(['namespace' => 'Plugins'], function () {

    /*
    +---------------------------------------------------------------------------+
    | Plugins routes
    +---------------------------------------------------------------------------+
    | Page plugin
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Post\Controllers'], function () {

      Route::resource('post', 'PostController');

    });

    /*
    +---------------------------------------------------------------------------+
    | News
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'News\Controllers'], function () {

      Route::resource('news', 'NewsController');

      Route::resource('/news/archive', 'SettingController', ['only' => 'index',
        'names' => [
          'index' => 'news.archive.index'
        ]
      ]);
      Route::resource('news/setting', 'SettingController', [
        'names' => [
          'index' => 'news.setting.index',
          'create' => 'news.setting.create',
          'edit' => 'news.setting.edit',
          'store' => 'news.setting.store',
          'update' => 'news.setting.update',
          'destroy' => 'news.setting.destroy',
        ]
      ]);

    });

    /*
    +---------------------------------------------------------------------------+
    | Foto
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Photo\Controllers'], function () {

      Route::post('/photo/multiupload/{photo?}', 'PhotoController@multiUpload')->name('photo.multiupload');

      Route::get('/photo/{photo}/create', 'PhotoController@create')->name('photo.createintoalbum');

      Route::post('/photo/{photo}/create', 'PhotoController@store');

      Route::post('/photo/{photo}/edit', 'PhotoController@update');

      Route::resource('photo', 'PhotoController');

    });

    /*
    +---------------------------------------------------------------------------+
    | Link
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Link\Controllers'], function () {

      Route::resource('link', 'LinkController');

    });

    /*
    +---------------------------------------------------------------------------+
    | Documenti
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Document\Controllers'], function () {

      Route::resource('document', 'DocumentController');

    });

    /*
    +---------------------------------------------------------------------------+
    | Modelli
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Pattern\Controllers'], function () {

      Route::resource('pattern', 'PatternController');

    });

    /*
    +---------------------------------------------------------------------------+
    | Modelli
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Template\Controllers'], function () {

      Route::resource('template', 'LayoutController');

    });

    /*
    +---------------------------------------------------------------------------+
    | Mail
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Mail\Controllers'], function () {

      Route::get('/mail/{mail_section}', 'MailController@index')->where('mail_section', '[A-Za-z]+')->name('mail.section');

      Route::post('/mail/create/attachments', 'MailController@uploadAttachment')->name('mail.attachment');

      Route::resource('mail', 'MailController');

    });

    /*
    +---------------------------------------------------------------------------+
    | Form
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Form\Controllers'], function () {

      Route::resource('form', 'FormController');

    });

    /*
    +---------------------------------------------------------------------------+
    | Category
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Category\Controllers'], function () {

      Route::resource('category', 'CategoryController');

    });

    /*
    +---------------------------------------------------------------------------+
    | Language
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Language\Controllers'], function () {

      Route::resource('language', 'LanguageController');

    });

    /*
    +---------------------------------------------------------------------------+
    | Setting
    +---------------------------------------------------------------------------+
    */
    Route::group(['namespace' => 'Setting\Controllers'], function () {

      Route::resource('setting', 'SettingController');

    });

  });

});
