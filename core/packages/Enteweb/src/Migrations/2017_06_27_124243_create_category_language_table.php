<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryLanguageTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('category_language', function (Blueprint $table) {
      $table->integer('category_id')->unsigned()->index();

      $table->integer('language_id')->unsigned()->index();

      /**
      * Foreignkeys section
      */
      $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

      $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('category_language');
  }
}
