<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociablesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('associables', function (Blueprint $table) {

      $table->integer('post_id')->unsigned()->index();

      $table->integer('associable_id')->unsigned()->index();

      $table->string('associable_type');

      /**
      * Foreignkeys section
      */
      $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('associables');
  }
}
