<?php

namespace Enteweb\Plugins\Form\Controllers;

/*
* Laravel classes
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App;

/*
* Models
*/
use Enteweb\Plugins\Language\Models\Language;
use Enteweb\Plugins\Post\Models\Post;
use Enteweb\Plugins\Form\Models\Form;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $forms = Post::getFullRows('form');

      return view('form::index', compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $form = new Post;

      return view('form::create', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if (array_key_exists('cancell', $request->all())) {

        return redirect()->route('enteweb.form.index');

      }

      $post = new Post;

      $form = new Form;

      $validator = Validator::make($request->all(), array_merge($post->rules, $form->rules));

      if ($validator->fails()) {
        return redirect()
        ->back()
        ->withErrors($validator)
        ->withInput();
      }

      if($request->input('parent_id') == 'NULL'){
        $request->merge(['parent_id' => NULL]);
      }

      $post->fill($request->all());
      $post->save();

      $request->request->add(['post_id' => $post->id]);

      $form->create($request->all());

      /*
      * Associazione lingua col post creato
      */
      $language = Language::currentLanguage(App::getLocale())->first();
      $post->languages()->attach($language->id);

      /*
      * Salvataggio categorie associate
      */
      $post->categories()->attach($request->input('category'));

      /*
      * Salvataggio gruppi associati
      */
      $post->groups()->attach($request->input('group'));

      Session::flash('saved', trans('enteweb.resource_create_success'));

      if (array_key_exists('save_exit', $request->all())) {

        return redirect()->route('enteweb.form.index');

      }else{

        return redirect()->route('enteweb.form.edit', $post->id);

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $form = Post::findOrFail($id)->getFullRow();

      return view('form::edit', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if (array_key_exists('cancell', $request->all())) {

        return redirect()->route('enteweb.form.index');

      }

      $post = Post::findOrFail($id);

      $form = $post->form()->first();

      $post->rules['slug'] = 'required';

      $validator = Validator::make($request->all(), array_merge($post->rules, $form->rules));

      if ($validator->fails()) {
        return redirect()
        ->back()
        ->withErrors($validator)
        ->withInput();
      }

      if($request->input('parent_id') == 'NULL'){
        $request->merge(['parent_id' => NULL]);
      }

      $post->update($request->all());

      $form->update($request->all());

      /*
      * Salvataggio categorie associate
      */
      $post->categories()->sync($request->input('category'));

      /*
      * Salvataggio gruppi associati
      */
      $post->groups()->sync($request->input('group'));

      Session::flash('saved', trans('enteweb.resource_edit_success'));

      if (array_key_exists('save_exit', $request->all())) {

        return redirect()->route('enteweb.form.index');

      }else{

        return redirect()->route('enteweb.form.edit', $post->id);

      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
