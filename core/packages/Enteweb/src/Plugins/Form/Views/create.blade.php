@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/jqueryui/jquery-ui.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/switchery/dist/switchery.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropzone-master/dist/dropzone.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css') }}" />
@endpush

@section('page_title')
  @lang('enteweb.forms')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.new_form')</strong></h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          @lang('enteweb.breadcrumb_here')
          <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
          <li>@lang('enteweb.forms')</li>
          <li>@lang('enteweb.new_form')</strong></li>
        </ol>
      </div>
    </div>
  @endsection

  @section('content')

    <section>
      <div class="sttabs tabs-style-underline">
        <nav>
          <ul>
            <li><a href="#content" class="fa fa-file-text-o"><span> @lang('enteweb.content')</span></a></li>
            <li><a href="#form" class="fa fa-list"><span> @lang('enteweb.build_your_form')</span></a></li>
            <li><a href="#email" class="fa fa-envelope-o"><span> @lang('enteweb.email')</span></a></li>
            <li><a href="#groups" class="fa fa-users"><span> @lang('enteweb.groups')</span></a></li>
          </ul>
        </nav>

        {!! Form::bsOpen($form, 'store', null, '') !!}

        {{ Form::hidden('post_type', 'form') }}
        {{ Form::hidden('created_by', Auth::user()->name) }}

        <div class="content-wrap">
          <section id="content">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  @include('enteweb::Includes.error_display')

                  <div class="row">
                    {{ Form::bsCheckbox('visible', 1, old('visible') ? old('visible') : true, ['width' => '1']) }}
                    {{ Form::bsLang('language') }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('title', old('title'), ['width' => 6]) }}
                    {{ Form::bsText('subtitle', old('subtitle'), ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('slug', old('slug'), ['width' => 6]) }}
                    {{ Form::bsText('view', old('view'), ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsNumber('order', old('order'), ['width' => '12']) }}
                  </div>

                  <div class="row">
                    {{ Form::bsMultiSelect('category[]', old('category_id'), $form->getCategories(), ['width' => '12'], 'Associa le categorie') }}
                  </div>

                  <div class="row">
                    {!! Form::bsBtnSave() !!}
                  </div>

                </div>
              </div>
            </div>
          </section>
          <section id="form">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  <div class="row">
                    {{ Form::bsCreateForm('code') }}
                  </div>

                  <div class="row">
                    {!! Form::bsBtnSave() !!}
                  </div>

                </div>
              </div>
            </div>
          </section>
          <section id="email">
            <div class="row">
              <!-- Left sidebar -->
              <div class="col-md-12">
                <div class="white-box">
                  <div class="row">
                    <div class="col-lg-12 col-md-9 col-sm-8 col-xs-12">
                      <div class="form-group">
                        <input class="form-control" name="from" value="{{ Auth::user()->email }}" placeholder="@lang('enteweb.from')"> </div>
                        <div class="form-group">
                          <input class="form-control" name="to" placeholder="@lang('enteweb.to')"> </div>
                          <div class="form-group">
                            <input class="form-control" name="subject" placeholder="@lang('enteweb.subject')"> </div>
                            <div class="form-group">
                              <textarea class="textarea_editor form-control" name="text" rows="15" placeholder="@lang('enteweb.enter_text')"></textarea>
                            </div>
                            <h4><i class="fa fa-link"></i> @lang('enteweb.attachment')</h4>
                            <form action="#" class="dropzone">
                              <div class="fallback">
                                <input name="file" type="file" multiple /> </div>
                              </form>
                              <hr>
                              <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> @lang('enteweb.send')</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="groups">
                    <h3>@lang('enteweb.group_access_resource')</h3>

                    {{ Form::bsGroupSelect('group[]', null, $form->getGroupsByPermission()) }}

                    {!! Form::bsBtnSave() !!}

                  </section>
                </div>

                {{ Form::close() }}

              </div>
            </section>

          @endsection

          @push('scripts')
            <script src="{{ asset('/themes/Backoffice/src/assets/js/cbpFWTabs.js') }}"></script>
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/jqueryui/jquery-ui.min.js') }}"></script>
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
            <script type="text/javascript" src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
            <script type="text/javascript" src="{{ asset('/themes/Backoffice/src/assets/js/includes/post.js') }}"></script>
            <script type="text/javascript" src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/formbuilder/dist/form-builder.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/formbuilder/dist/form-render.min.js') }}"></script>
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js') }}"></script>
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js') }}"></script>

            <script type="text/javascript">
            jQuery(function($) {
              $('.textarea_editor').wysihtml5();
              var options = {
                disabledActionButtons: ['data','save'],

                roles: {
                  1: 'SuperAdmin',
                  2: 'Admin',
                  3: 'Supervisore',
                  4: 'Segretario',
                  5: 'Utente',
                }
              };
              var fbEditor = document.getElementById('build-wrap');
              var formBuilder = $(fbEditor).formBuilder(options);

              $('.submit').on('click', function () {
                var formData = formBuilder.actions.getData('json');
                $('#code').val(formData);
              });
            });
            </script>
          @endpush
