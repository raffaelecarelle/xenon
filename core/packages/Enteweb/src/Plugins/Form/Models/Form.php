<?php

namespace Enteweb\Plugins\Form\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
  protected $table = 'forms';

  protected $fillable = ['code', 'created_by', 'updated_by', 'post_id'];

  public $rules = [];

}
