@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('page_title')
  @lang('enteweb.languages')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.edit_language')</strong>
        {!! Form::bsBtnCreate(trans('enteweb.new_language')) !!}
      </h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        @lang('enteweb.breadcrumb_here')
        <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
        <li>@lang('enteweb.languages')</li>
        <li>@lang('enteweb.edit_language')</strong></li>
      </ol>
    </div>
  </div>
@endsection

@section('content')

  <section>
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box p-l-20 p-r-20">
          <div class="row">
            <div class="col-md-12">

              {!! Form::bsOpen($row, 'update', null, 'form-material') !!}

              {{ Form::hidden('updated_by', Auth::user()->name) }}

              @include('enteweb::Includes.error_display')

              <div class="row">
                {{ Form::bsText('title', old('title') ? old('title') : $row->title, ['width' => '12']) }}
              </div>

              <div class="row">
                {{ Form::bsSelect('code', old('code') ? old('code') : $row->code, Config('countries'), ['width' => '12']) }} <br />
              </div>

              <div class="row">
                {!! Form::bsBtnSave() !!}
              </div>

              {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection

@push('scripts')
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
  <script>
  jQuery(document).ready(function() {
    // For select 2
    $(".select2").select2();

    @include('enteweb::Includes.saved-alert')
  });
  </script>
@endpush
