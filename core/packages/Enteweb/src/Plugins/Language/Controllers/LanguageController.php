<?php

namespace Enteweb\Plugins\Language\Controllers;

/*
 * Laravel classes
 */
use App;
use Validator;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/*
 * Models
 */
use Enteweb\Plugins\Language\Models\Language;

/*
* Extend classes
*/
use Extend\Traits\CrudTrait;
use Extend\Libraries\SweetAlert;

class LanguageController extends Controller {
  use CrudTrait;

  public function index(){

    $languages = Language::all();

    $view = $this->getView();

    return view($view)->with('languages', $languages);
  }

  /**
  * Salva una nuova lingua
  * @method store
  * @param  Request $request
  * @return void
  */
  public function store(Request $request) {

    if (array_key_exists('cancell', $request->all())) {

      return redirect()->route('enteweb.language.index');

    }

    $language = new Language;

    $validator = Validator::make($request->all(), $language->rules);

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    $language->fill($request->all());
    $language->save();

    Session::flash('saved', trans('enteweb.resource_create_success'));

    if (array_key_exists('save_exit', $request->all())) {

      return redirect()->route('enteweb.language.index');

    }else{

      return redirect()->route('enteweb.language.edit', $language->id);

    }
  }

  /**
  * Aggiorna una specifica lingua
  * @method update
  * @param  Request $request, $id
  * @return void
  */
  public function update(Request $request, $id) {

    if (array_key_exists('cancell', $request->all())) {

      return redirect()->route('enteweb.language.index');

    }

    $language = language::find($id);

    $language->rules['code'] = 'required';

    $validator = Validator::make($request->all(), $language->rules);

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    $language->fill($request->all());
    $language->save();

    Session::flash('saved', trans('enteweb.resource_edit_success'));

    if (array_key_exists('save_exit', $request->all())) {

      return redirect()->route('enteweb.language.index');

    }else{

      return redirect()->route('enteweb.language.edit', $language->id);

    }
  }
}
