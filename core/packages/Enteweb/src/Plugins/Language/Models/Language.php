<?php

namespace Enteweb\Plugins\Language\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * Models
 */
use Enteweb\Plugins\News\Models\NewsSetting;
use Enteweb\Plugins\Category\Models\Category;

class Language extends Model
{
  protected $table = 'languages';

  protected $fillable = [
    'title',
    'code',
    'created_by',
    'updated_by'
  ];

  public $rules = [
    'title' => 'required',
    'code' => 'unique:languages|required'
  ];

  /**
  * Get all of the posts that are assigned this language..
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function posts(){

    return $this->morphToMany(Post::class, 'associable');

  }

  public function newsSettings() {
    return $this->morphedByMany(NewsSetting::class, 'languageble');
  }

  public function scopeCurrentLanguage($query, $languageCode) {
    $query->where('code', $languageCode);
  }
}
