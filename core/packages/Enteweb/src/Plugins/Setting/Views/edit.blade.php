@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/switchery/dist/switchery.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
@endpush

@section('page_title')
  @lang('enteweb.settings')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.edit_settings')</strong>
        {!! Form::bsBtnCreate(trans('enteweb.new_setting')) !!}
      </h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        @lang('enteweb.breadcrumb_here')
        <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
        <li>@lang('enteweb.settings')</li>
        <li>@lang('enteweb.edit_settings')</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')

  <section>
    <div class="sttabs tabs-style-underline">
      <nav>
        <ul>
          <li><a href="#general"><span> @lang('enteweb.general')</span></a></li>
          <li><a href="#metatag"><span> @lang('enteweb.metatag')</span></a></li>
          <li><a href="#theme"><span> @lang('enteweb.theme')</span></a></li>
        </ul>
      </nav>

      {!! Form::bsOpen($setting, 'update', null, 'form-material') !!}


      <div class="content-wrap">
        <section id="general">
          <div class="row">
            <div class="col-sm-12">
              <div class="white-box p-l-20 p-r-20">

                @include('enteweb::Includes.error_display')

                <div class="row">
                  {{ Form::bsCheckbox('active', 1, $setting->active, ['width' => '1']) }}
                  {{ Form::bsLang('language') }}
                </div>

                <div class="row">
                  {{ Form::bsText('setting_title', old('setting_title') ? old('setting_title') : $setting->setting_title, ['width' => '12']) }}
                </div>

                <div class="row">
                  {{ Form::bsText('site_title', old('site_title') ? old('site_title') : $setting->site_title, ['width' => '4']) }}
                  {{ Form::bsText('tagline', old('tagline') ? old('tagline') : $setting->tagline, ['width' => '4']) }}
                  {{ Form::bsText('site_address', old('site_address') ? old('site_address') : $setting->site_address, ['width' => '4']) }}
                </div>

                <div class="row">
                  {{ Form::bsSelect('new_user_default_role', old('new_user_default_role') ? old('new_user_default_role') : $setting->new_user_default_role, $setting->getPermissions()->pluck('title', 'type') ,['width' => '12']) }}
                </div>
                <div class="row">
                  {{ Form::bsSelect('timezone', $setting->timezone, config('settings.timezone'), ['width' => '4']) }}
                  {{ Form::bsSelect('date_format', $setting->date_format, config('settings.date_format'), ['width' => '4']) }}
                  {{ Form::bsSelect('time_format', $setting->time_format, config('settings.time_format'), ['width' => '4']) }}
                </div>

                <div class="row">
                  {{ Form::bsSelect('weeks_start_on', $setting->weeks_start_on, config('settings.weeks_start_on'), ['width' => '12']) }}
                </div>

                <div class="row">
                  {!! Form::bsBtnSave() !!}
                </div>

              </div>
            </div>
          </div>
        </section>
        <section id="metatag">
          <div class="row">
            <div class="col-sm-12">
              <div class="white-box p-l-20 p-r-20">

                <div class="row">
                  {{ Form::bsText('meta_tag_title', old('meta_tag_title') ? old('meta_tag_title') : $setting->meta_tag_title, ['width' => '6']) }}
                  {{ Form::bsText('meta_tag_author', old('meta_tag_author') ? old('meta_tag_author') : $setting->meta_tag_author, ['width' => '6']) }}
                </div>

                <div class="row">
                  {{ Form::bsText('meta_tag_description', old('meta_tag_description') ? old('meta_tag_description') : $setting->meta_tag_description, ['width' => '6']) }}
                  {{ Form::bsTags('meta_tag_keywords', old('meta_tag_keywords') ? old('meta_tag_keywords') : $setting->meta_tag_keywords, ['width' => '6']) }}
                </div>

                {!! Form::bsBtnSave() !!}

              </div>
            </div>
          </div>
        </section>

        <section id="theme">
          <div class="row">
            <div class="col-lg-12">
              <div class="white-box">
                Da Implementare

              </div>
            </div>
          </div>
        </section>
      </div>

      {{ Form::close() }}

    </div>

  </section>

@endsection

@push('scripts')
  <script src="{{ asset('/themes/Backoffice/src/assets/js/cbpFWTabs.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
  <script type="text/javascript">
  (function() {
    [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
      new CBPFWTabs(el);
    });
  })();
  jQuery(document).ready(function() {
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
      new Switchery($(this)[0], $(this).data());
    });

    // For select 2
    $(".select2").select2();

    @include('enteweb::Includes.saved-alert')
  });
  </script>
@endpush
