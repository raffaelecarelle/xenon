<?php

namespace Enteweb\Plugins\Setting\Models;

/*
* laravel classes
 */
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/*
* Models
*/
use LiveUsers\Models\Permission;
use LiveUsers\Models\Application;
use Enteweb\Plugins\Language\Models\Language;

/*
* Extend classes
 */
use Extend\Scopes\LanguagesScope;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = [
      'setting_title',
      'site_title',
      'tagline',
      'site_address',
      'timezone',
      'date_format',
      'time_format',
      'weeks_start_on',
      'meta_tag_title',
      'meta_tag_author',
      'meta_tag_description',
      'meta_tag_keywords',
      'application_id',
      'active',
      'created_by',
      'updated_by',
      'setting_title',
      'new_user_default_role'
    ];

    /**
    * The "booting" method of the model.
    *
    * @return void
    */
    protected static function boot()
    {
      parent::boot();

      static::addGlobalScope(new LanguagesScope);
    }

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    ///////               RELATIONSHIPS                               ////////
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    /**
  	* application
  	*
  	* @return  \Illuminate\Support\Collection;
  	*/
  	public function application() {
  		return $this->belongsTo(Application::class);
  	}

    public function languages() {
      return $this->belongsToMany(Language::class);
    }

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    ///////                      SCOPES                               ////////
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    public function scopeActive($query) {
      $query->where('active', 1);
    }

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    ///////               HELPER FUNCTIONS                            ////////
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    public function getPermissions() {
      return Permission::untilUserType()->get();
    }
}
