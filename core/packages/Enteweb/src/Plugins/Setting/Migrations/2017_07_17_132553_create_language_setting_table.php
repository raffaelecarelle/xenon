<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguageSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('language_setting', function (Blueprint $table) {

        $table->integer('setting_id')->unsigned()->index();

        $table->integer('language_id')->unsigned()->index();

        /**
        * Foreignkeys section
        */
        $table->foreign('setting_id')->references('id')->on('settings')->onDelete('cascade');

        $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('language_setting');
    }
}
