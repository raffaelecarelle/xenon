<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('settings', function (Blueprint $table) {

      $table->increments('id');

      $table->string('setting_title');

      $table->string('site_title')->nullable();

      $table->string('tagline')->nullable();

      $table->string('site_address')->nullable();

      $table->integer('new_user_default_role')->unsigned()->nullable();

      $table->string('timezone')->nullable();

      $table->string('date_format')->nullable();

      $table->string('time_format')->nullable();

      $table->string('weeks_start_on')->nullable();

      $table->string('meta_tag_title')->nullable();

      $table->string('meta_tag_author')->nullable();

      $table->string('meta_tag_description')->nullable();

      $table->string('meta_tag_keywords')->nullable();

      $table->string('created_by')->nullable();

      $table->string('updated_by')->nullable();

      $table->integer('application_id')->unsigned();

      $table->boolean('active')->nullable()->default(0);

      $table->timestamps();

      /**
      * Foreignkeys section
      */
      $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');

      $table->foreign('new_user_default_role')->references('id')->on('permissions')->onDelete('set null');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('settings');
  }
}
