<?php

namespace Enteweb\Plugins\Setting\Controllers;

/*
* Laravel classes
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Validator;
use Auth;

/*
* Models
*/
use Enteweb\Plugins\Setting\Models\Setting;
use LiveUsers\Models\Application;

/*
* Extend classes
*/
use Extend\Traits\CrudTrait;

class SettingController extends Controller {

  use CrudTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $settings = Setting::all();

      return view('setting::index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

      $setting = new Setting;

      return view('setting::create', compact('setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      if (array_key_exists('cancell', $request->all())) {

        return redirect()->route('lenteweb.setting.index');

      }

      $setting = new Setting;

      $application = Application::enteweb()->first();

      if($request->input('active')){

        $setting_active = Setting::active()->first();

        if($setting_active){

          $setting_active->update(['active' => 0]);

        }
      }

      $request->request->add(['created_by' => Auth::user()->name]);

      $request->request->add(['application_id' => $application->id]);

      $setting->create($request->all());

      Session::flash('saved', trans('enteweb.resource_create_success'));

      if (array_key_exists('save_exit', $request->all())) {

        return redirect()->route('enteweb.setting.index');

      }else{

        return redirect()->route('enteweb.setting.edit', $setting->id);

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $setting = Setting::findOrFail($id);

      return view('setting::edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

      if (array_key_exists('cancell', $request->all())) {

        return redirect()->route('liveusers.application.index');

      }

      $setting = Setting::findOrFail($id);

      if($request->input('active')){

        $setting_active = Setting::active()->where('id', '!=', $id)->first();

        if(NULL != $setting_active){

          $setting_active->update(['active' => 0]);

        }
      }

      $request->request->add(['updated_by' => Auth::user()->name]);

      $setting->update($request->all());

      Session::flash('saved', trans('enteweb.setting.index'));

      if (array_key_exists('save_exit', $request->all())) {

        return redirect()->route('enteweb.setting.index');

      }else{

        return redirect()->route('enteweb.setting.edit', $setting->id);

      }
    }
}
