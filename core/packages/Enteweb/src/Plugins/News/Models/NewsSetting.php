<?php

namespace Enteweb\Plugins\News\Models;

/*
 * Laravel classes
 */
use Illuminate\Database\Eloquent\Model;

/*
 * Global scopes
 */
use Extend\Scopes\LanguagesScope;

/*
 * Models
 */
use Enteweb\Plugins\Language\Models\Language;

class NewsSetting extends Model
{
  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();

    static::addGlobalScope(new LanguagesScope);
  }

  protected $table = 'news_settings';

  protected $fillable = [
    'title',
    'active',
    'n_visible_preview',
    'n_item_x_page_active',
    'n_item_x_page_archive',
    'n_item_x_page_search',
    'max_n_page',
    'template_list_preview',
    'template_list',
    'template',
    'template_search',
    'format_date_list',
    'format_date_page',
    'format_date_preview',
    'locale',
    'preview_page_list_active',
    'preview_page_view_active',
    'preview_page_list_archive',
    'preview_page_view_archive',
    'preview_page_search',
    'preview_page_search_list',
    'preview_page_search_view',
    'created_by',
    'updated_by'
  ];

  public $rules = [
    'title' => 'required',
    'n_visible_preview' => 'required',
    'n_item_x_page_active' => 'required',
    'n_item_x_page_archive' => 'required',
    'n_item_x_page_search' => 'required',
    'max_n_page' => 'required',
    'template_list_preview' => 'required',
    'template_list' => 'required',
    'template' => 'required',
    'template_search' => 'required',
    'preview_page_list_active' => 'required',
    'preview_page_view_active' => 'required',
    'preview_page_list_archive' => 'required',
    'preview_page_view_archive' => 'required',
    'preview_page_search' => 'required',
    'preview_page_search_list' => 'required',
    'preview_page_search_view' => 'required',
  ];

  public function languages() {
    return $this->belongsToMany(Language::class);
  }

  public function scopeActive($query){
    $query->where('active', 1);
  }
}
