<?php

namespace Enteweb\Plugins\News\Models;

/*
* Laravel classes
*/
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/*
* Models
*/
use Enteweb\Plugins\Setting\Models\Setting;

class News extends Model
{
  protected $table = 'news';

  protected $fillable = [
    'published_at',
    'end_published_at',
    'published_in_home_at',
    'end_published_in_home_at',
    'post_id'
  ];

  protected $dates = [
    'published_at',
    'end_published_at',
    'published_in_home_at',
    'end_published_in_home_at'
  ];

  public static $rules = [
    'published_at' => 'required',
    'end_published_at' => 'required',
    'published_in_home_at' => 'required',
    'end_published_in_home_at' => 'required'
  ];

  protected $setting;

  public function __construct(){
    $this->setting = Setting::active()->first();
  }

  /**
  * Scrive l'attributo published_at nel formato specificato nella tabella settings
  * @method setPublishedAtAttribute
  * @param  date $date
  */
  public function setPublishedAtAttribute($date){
    return $this->attributes['published_at'] = Carbon::parse($date);
  }

  /**
  * Legge l'attributo end_published_at nel formato specificato nella tabella settings
  * @method getPublishedAtAttribute
  * @param  date $date
  */
  public function getPublishedAtAttribute($date){
    return $this->asDate($date)->format($this->switchDateFormat($this->setting->date_format));
  }

  /**
  * Scrive l'attributo end_published_at nel formato specificato nella tabella settings
  * @method setEndPublishedAtAttribute
  * @param  date $date
  */
  public function setEndPublishedAtAttribute($date){
    return $this->attributes['end_published_at'] = Carbon::parse($date);
  }

  /**
  * Legge l'attributo end_published_at nel formato specificato nella tabella settings
  * @method getEndPublishedAtAttribute
  * @param  date $date
  */
  public function getEndPublishedAtAttribute($date){

    return $this->asDate($date)->format($this->switchDateFormat($this->setting->date_format));
  }

  /**
  * Scrive l'attributo published_in_home_at nel formato specificato nella tabella settings
  * @method setPublishedInHomeAtAttribute
  * @param  date $date
  */
  public function setPublishedInHomeAtAttribute($date){
    return $this->attributes['published_in_home_at'] = Carbon::parse($date);
  }

  /**
  * Legge l'attributo published_in_home_at nel formato specificato nella tabella settings
  * @method getPublishedInHomeAtAttribute
  * @param  date $date
  */
  public function getPublishedInHomeAtAttribute($date){
    return $this->asDate($date)->format($this->switchDateFormat($this->setting->date_format));
  }

  /**
  * Scrive l'attributo end_published_in_home_at nel formato specificato nella tabella settings
  * @method setEndPublishedInHomeAtAttribute
  * @param  date $date
  */
  public function setEndPublishedInHomeAtAttribute($date){
    return $this->attributes['end_published_in_home_at'] = Carbon::parse($date);
  }
  /**
  * Legge l'attributo end_published_in_home_at nel formato specificato nella tabella settings
  * @method getEndPublishedInHomeAtAttribute
  * @param  date $date
  */
  public function getEndPublishedInHomeAtAttribute($date){
    return $this->asDate($date)->format($this->switchDateFormat($this->setting->date_format));
  }

  public function switchDateFormat($dateFormat){

    switch ($dateFormat) {

      case 'dd-mm-yyyy':
      return 'd-m-Y';

      case 'yyyy-mm-dd':
      return 'Y-m-d';

      default:
      return 'd-m-Y';
    }
  }

}
