<?php

namespace Enteweb\Plugins\News\Controllers;

/*
* Laravel classes
*/
use App;
use Validator;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/*
 * Models
 */
use Enteweb\Plugins\News\Models\NewsSetting;
use Enteweb\Plugins\Language\Models\Language;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $settings = NewsSetting::all();

      return view('news::Setting.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $setting = new NewsSetting;

      return view('news::Setting.create', compact('setting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if (array_key_exists('cancell', $request->all())) {

        return redirect()->route('enteweb.news.setting.index');

      }

      $setting = new NewsSetting;

      $validator = Validator::make($request->all(), $setting->rules);

      if ($validator->fails()) {
        return redirect()
        ->back()
        ->withErrors($validator)
        ->withInput();
      }

      if($request->input('active')) {

        $activeSetting = NewsSetting::active()->first();

        if($activeSetting) {

          $activeSetting->update(['active' => 0]);

        }
      }

      $setting->fill($request->all());
      $setting->save();

      /*
      * Associazione lingua col post creato
      */
      $language = Language::currentLanguage(App::getLocale())->first();
      $setting->languages()->attach($language->id);

      Session::flash('saved', trans('enteweb.resource_create_success'));

      if (array_key_exists('save_exit', $request->all())) {

        return redirect()->route('enteweb.news.setting.index');

      }else{

        return redirect()->route('enteweb.news.setting.edit', $setting->id);

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $setting = NewsSetting::findOrFail($id);

      return view('news::Setting.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if (array_key_exists('cancell', $request->all())) {

        return redirect()->route('enteweb.news.setting.index');

      }

      $setting = NewsSetting::findOrFail($id);

      $validator = Validator::make($request->all(), $setting->rules);

      if ($validator->fails()) {
        return redirect()
        ->back()
        ->withErrors($validator)
        ->withInput();
      }

      if($request->input('active')) {

        $activeSetting = NewsSetting::active()->first();

        if($activeSetting) {

          $activeSetting->update(['active' => 0]);

        }
      }

      $setting->update($request->all());

      /*
      * Associazione lingua col post creato
      */
      $language = Language::currentLanguage('code', App::getLocale())->first();
      $setting->languages()->sync($language->id);

      Session::flash('saved', trans('enteweb.resource_edit_success'));

      if (array_key_exists('save_exit', $request->all())) {

        return redirect()->route('enteweb.news.setting.index');

      }else{

        return redirect()->route('enteweb.news.setting.edit', $setting->id);

      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      NewsSetting::findOrFail($id)->delete();
    }
}
