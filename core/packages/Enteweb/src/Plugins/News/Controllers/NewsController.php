<?php

namespace Enteweb\Plugins\News\Controllers;

/*
* Laravel classes
*/
use App;
use Validator;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

/*
 * Models
 */
use Enteweb\Plugins\Post\Models\Post;
use Enteweb\Plugins\News\Models\News;
use Enteweb\Plugins\Language\Models\Language;

class NewsController extends Controller {

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index($archive = false) {

    $news = Post::getFullRows('news');

    if($archive) {

      $newsFiltered = $news->filter(function($item, $key) {
        return Carbon::parse($item->end_published_at)->lt(Carbon::now());
      });

      $flag = 'Archiviata';

    } else {

      $newsFiltered = $news->filter(function($item, $key) {
        return Carbon::now()->between(Carbon::parse($item->published_at), Carbon::parse($item->end_published_at));
      });

      $flag = 'Pubblicata';

    }

    return view('news::News.index', compact('newsFiltered', 'flag'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create() {
    $news = new Post;

    return view('news::News.create', compact('news'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request) {

    if (array_key_exists('cancell', $request->all())) {

      return redirect()->route('enteweb.news.index');

    }

    $validator = Validator::make($request->all(), array_merge(Post::$rules, News::$rules));

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    $post = new Post;

    $post->fill($request->all());
    $post->save();

    $request->request->add(['post_id' => $post->id]);

    $news = new News;

    $news->fill($request->all());
    $news->save();

    /*
    * Associazione lingua col post creato
    */
    $currentLanguage = App::getLocale();
    $language = Language::where('code', $currentLanguage)->first();
    $post->languages()->attach($language->id);

    /*
    * Salvataggio categorie associate
    */
    $categories_ids = array_filter(explode(',', $request->input('category')));
    $post->categories()->attach($categories_ids);


    /*
    * Salvataggio link associati
    */
    $links_ids = array_filter(explode(',', $request->input('link')));
    $post->links()->attach($links_ids);


    /*
    * Salvataggio gruppi associati
    */
    $groups_ids = array_filter(explode(',', $request->input('group')));
    $post->groups()->attach($groups_ids);

    Session::flash('saved', trans('enteweb.resource_create_success'));

    if (array_key_exists('save_exit', $request->all())) {

      return redirect()->route('enteweb.news.index');

    }else{

      return redirect()->route('enteweb.news.edit', $post->id);

    }

  }

  /**
  * Mostra la lista delle news archiviate oppure manda all'index delle impostazioni
  *
  * @param
  * @return \Illuminate\Http\Response
  */
  public function show($flag)
  {
    if($flag == 'archive') {

      return $this->index(true);

    } else {

      return app(SettingController::class)->index();

    }
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id) {
    $post = Post::findOrFail($id);

    $news = $post->getFullRow();

    return view('news::News.edit', compact('news'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id) {

    if (array_key_exists('cancell', $request->all())) {

      return redirect()->route('enteweb.news.index');

    }

    $post = Post::findOrFail($id);

    Post::$rules['slug'] = 'required';

    $validator = Validator::make($request->all(), array_merge(Post::$rules, News::$rules));

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    $post->update($request->all());

    /*
    * Salvataggio categorie associate
    */
    $categories_ids = array_filter(explode(',', $request->input('category')));
    $post->categories()->sync($categories_ids);


    /*
    * Salvataggio link associati
    */
    $links_ids = array_filter(explode(',', $request->input('link')));
    $post->links()->sync($links_ids);


    /*
    * Salvataggio gruppi associati
    */
    $groups_ids = array_filter(explode(',', $request->input('group')));
    $post->groups()->sync($groups_ids);

    $news = News::where('post_id', $id)->first();

    $news = $news->update($request->all());

    Session::flash('saved', trans('enteweb.resource_edit_success'));

    if (array_key_exists('save_exit', $request->all())) {

      return redirect()->route('enteweb.news.index');

    }else{

      return redirect()->route('enteweb.news.edit', $post->id);

    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    Post::findOrFail($id)->delete();

    return redirect()->back();
  }
}
