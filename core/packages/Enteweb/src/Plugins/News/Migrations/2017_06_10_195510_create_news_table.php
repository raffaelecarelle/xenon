<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');

            $table->date('published_at');

            $table->date('end_published_at');

            $table->date('published_in_home_at')->nullable();

            $table->date('end_published_in_home_at')->nullable();

            $table->integer('post_id')->unsigned();

            $table->string('created_by')->nullable();

            $table->string('updated_by')->nullable();

            $table->timestamps();

            /**
            * Foreignkeys section
            */

            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
