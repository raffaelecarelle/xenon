<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_settings', function (Blueprint $table) {

            $table->increments('id');

            $table->string('title');

            $table->boolean('active');

            $table->integer('n_visible_preview');

            $table->integer('n_item_x_page_active');

            $table->integer('n_item_x_page_archive');

            $table->integer('n_item_x_page_search');

            $table->integer('max_n_page');

            $table->string('template_list_preview');

            $table->string('template_list');

            $table->string('template');

            $table->string('template_search');

            $table->string('format_date_list');

            $table->string('format_date_page');

            $table->string('format_date_preview');

            $table->string('locale');

            $table->string('preview_page_list_active');

            $table->string('preview_page_view_active');

            $table->string('preview_page_list_archive');

            $table->string('preview_page_view_archive');

            $table->string('preview_page_search');

            $table->string('preview_page_search_list');

            $table->string('preview_page_search_view');

            $table->string('created_by')->nullable();

            $table->string('updated_by')->nullable();

            $table->timestamps();

            /**
            * Foreignkeys section
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_settings');
    }
}
