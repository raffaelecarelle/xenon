<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguageNewsSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('language_news_setting', function (Blueprint $table) {

        $table->integer('news_setting_id')->unsigned()->index();

        $table->integer('language_id')->unsigned()->index();

        /**
        * Foreignkeys section
        */
        $table->foreign('news_setting_id')->references('id')->on('news_settings')->onDelete('cascade');

        $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('language_news_setting');
    }
}
