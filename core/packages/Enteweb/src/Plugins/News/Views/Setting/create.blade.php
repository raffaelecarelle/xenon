@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/switchery/dist/switchery.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endpush

@section('page_title')
  @lang('enteweb.news')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.new_news')</strong></h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          @lang('enteweb.breadcrumb_here')
          <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
          <li>@lang('enteweb.news')</li>
          <li>@lang('enteweb.new_news')</strong></li>
        </ol>
      </div>
    </div>
  @endsection

  @section('content')

    <section>
      <div class="sttabs tabs-style-underline">
        <nav>
          <ul>
            <li><a href="#general" class="fa fa-file-text-o"><span> @lang('enteweb.general')</span></a></li>
            <li><a href="#templates" class="fa fa-th"><span> @lang('enteweb.templates')</span></a></li>
            <li><a href="#format_dates" class="fa fa-calendar"><span> @lang('enteweb.format_dates')</span></a></li>
            <li><a href="#pages_for_news_actived" class="fa fa-fire"><span> @lang('enteweb.pages_for_news_actived')</span></a></li>
            <li><a href="#pages_for_news_archived" class="fa fa-archive"><span> @lang('enteweb.pages_for_news_archived')</span></a></li>
            <li><a href="#pages_for_news_searched" class="fa fa-search"><span> @lang('enteweb.pages_for_news_searched')</span></a></li>
          </ul>
        </nav>

        {!! Form::bsOpen($setting, 'store', null, 'form-material') !!}

        {{ Form::hidden('created_by', Auth::user()->name) }}

        <div class="content-wrap">
          <section id="general">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  @include('enteweb::Includes.error_display')

                  <div class="row">
                    {{ Form::bsCheckbox('active', 1, old('active') ? old('active') : false, ['width' => '1']) }}
                    {{ Form::bsLang('language') }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('title', old('title'), ['width' => 12]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsNumber('n_visible_preview', old('n_visible_preview'), ['width' => 6]) }}
                    {{ Form::bsNumber('n_item_x_page_active', old('n_item_x_page_active'), ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsNumber('n_item_x_page_archive', old('n_item_x_page_archive'), ['width' => '6']) }}
                    {{ Form::bsNumber('n_item_x_page_search', old('n_item_x_page_search'), ['width' => '6']) }}
                  </div>

                  <div class="row">
                    {{ Form::bsNumber('max_n_page', old('max_n_page'), ['width' => '12']) }}
                  </div>

                  <div class="row">
                    {!! Form::bsBtnSave() !!}
                  </div>

                </div>
              </div>
            </div>
          </section>

          <section id="templates">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  <div class="row">
                    {{ Form::bsText('template_list_preview', old('template_list_preview'), ['width' => 6]) }}
                    {{ Form::bsText('template_list', old('template_list'), ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('template', old('template'), ['width' => 6]) }}
                    {{ Form::bsText('template_search', old('template_search'), ['width' => 6]) }}
                  </div>

                  {!! Form::bsBtnSave() !!}

                </div>
              </div>
            </div>
          </section>

          <section id="format_dates">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  <div class="row">
                    {{ Form::bsSelect('format_date_list', old('format_date_list'), config('settings.date_format'), ['width' => 6]) }}
                    {{ Form::bsSelect('format_date_page', old('format_date_page'), config('settings.date_format'), ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsSelect('format_date_preview', old('format_date_preview'), config('settings.date_format'), ['width' => 6]) }}
                    {{ Form::bsSelect('locale', old('locale'), config('settings.date_format'), ['width' => 6]) }}
                  </div>

                  {!! Form::bsBtnSave() !!}

                </div>
              </div>
            </div>
          </section>

          <section id="pages_for_news_actived">

            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  <div class="row">
                    {{ Form::bsText('preview_page_list_active', old('preview_page_list_active'), ['width' => 6]) }}
                    {{ Form::bsText('preview_page_view_active', old('preview_page_view_active'), ['width' => 6]) }}
                  </div>

                  {!! Form::bsBtnSave() !!}

                </div>
              </div>
            </div>

          </section>
          <section id="pages_for_news_archived">

            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  <div class="row">
                    {{ Form::bsText('preview_page_list_archive', old('preview_page_list_archive'), ['width' => 6]) }}
                    {{ Form::bsText('preview_page_view_archive', old('preview_page_view_archive'), ['width' => 6]) }}
                  </div>

                  {!! Form::bsBtnSave() !!}

                </div>
              </div>
            </div>

          </section>

          <section id="pages_for_news_searched">

            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  <div class="row">
                    {{ Form::bsText('preview_page_search', old('preview_page_search'), ['width' => 6]) }}
                    {{ Form::bsText('preview_page_search_list', old('preview_page_search_list'), ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('preview_page_search_view', old('preview_page_search_view'), ['width' => 12]) }}
                  </div>

                  {!! Form::bsBtnSave() !!}

                </div>
              </div>
            </div>

          </section>
        </div>

        {{ Form::close() }}

      </div>
    </section>

  @endsection

  @push('scripts')

    <script src="{{ asset('/themes/Backoffice/src/assets/js/cbpFWTabs.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">

    jQuery(document).ready(function() {

      (function() {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
          new CBPFWTabs(el);
        });
      })();
      // Switchery
      var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
      $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
      });

      //Bootstrap-TouchSpin
      $(".vertical-spin").TouchSpin({
        verticalbuttons: true,
        verticalupclass: 'ti-plus',
        verticaldownclass: 'ti-minus'
      });
      var vspinTrue = $(".vertical-spin").TouchSpin({
        verticalbuttons: true
      });
      if (vspinTrue) {
        $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
      }

      // For select 2
      $(".select2").select2();
    });


    </script>

  @endpush
