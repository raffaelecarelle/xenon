@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/switchery/dist/switchery.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('page_title')
  @lang('enteweb.news')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.edit_news')</strong></h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          @lang('enteweb.breadcrumb_here')
          <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
          <li>@lang('enteweb.news')</li>
          <li>@lang('enteweb.edit_news')</strong></li>
        </ol>
      </div>
    </div>
  @endsection

  @section('content')

    <section>
      <div class="sttabs tabs-style-underline">
        <nav>
          <ul>
            <li><a href="#content" class="fa fa-file-text-o"><span> @lang('enteweb.content')</span></a></li>
            <li><a href="#dates" class="fa fa-calendar"><span> @lang('enteweb.dates')</span></a></li>
            <li><a href="#seo" class="fa fa-eye"><span> @lang('enteweb.seo')</span></a></li>
            <li><a href="#groups" class="fa fa-users"><span> @lang('enteweb.groups')</span></a></li>
          </ul>
        </nav>

        {!! Form::bsOpen($news, 'update', null, 'form-material') !!}

        {{ Form::hidden('post_type', 'news') }}
        {{ Form::hidden('updated_by', Auth::user()->name) }}

        <div class="content-wrap">
          <section id="content">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  @include('enteweb::Includes.error_display')

                  <div class="row">
                    {{ Form::bsCheckbox('visible', 1, old('visible') ? old('visible') : true, ['width' => '1']) }}
                    {{ Form::bsLang('language') }}
                  </div>


                  <div class="row">
                    {{ Form::bsText('title', old('title') ? old('title') : $news->title, ['width' => 6]) }}
                    {{ Form::bsText('subtitle', old('subtitle') ? old('subtitle') : $news->subtitle, ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('slug', old('slug') ? old('slug') : $news->slug, ['width' => 6]) }}
                    {{ Form::bsText('view', old('view') ? old('view') : $news->view, ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsNumber('order', old('order') ? old('order') : $news->order, ['width' => '12']) }}
                  </div>

                  <div class="row">
                    {{ Form::bsMultiSelect('category[]', old('category_id[]') ? old('category_id[]') : $news->categories()->get()->pluck('id', 'title'), $news->getCategories(), ['width' => '12'], 'Associa le categorie') }}
                  </div>

                  <div class="row">
                    {{ Form::bsMultiSelect('link[]', old('link[]') ? old('link[]') : $news->links()->get()->pluck('id', 'title'), $news->getFullRows('link')->pluck('title', 'id'),['width' => 12], 'Associa i link') }}
                  </div>

                  <div class="row">
                    {{ Form::bsTextarea('text', old('text') ? old('text') : $news->text) }}
                  </div>

                  <div class="row">
                    {!! Form::bsBtnSave() !!}
                  </div>

                </div>
              </div>
            </div>
          </section>

          <section id="dates">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  <div class="row m-b-15">
                    {{ Form::bsDatePicker('published_at', old('published_at') ? old('published_at') : $news->published_at , ['width' => '11']) }}
                  </div>

                  <div class="row m-b-15">
                  {{ Form::bsDatePicker('end_published_at', old('end_published_at') ? old('end_published_at') : $news->end_published_at, ['width' => '11']) }}
                  </div>

                  <div class="row m-b-15">
                    {{ Form::bsDatePicker('published_in_home_at', old('published_in_home_at') ? old('published_in_home_at') : $news->published_in_home_at, ['width' => '11']) }}
                  </div>

                  <div class="row m-b-15">
                    {{ Form::bsDatePicker('end_published_in_home_at', old('end_published_in_home_at') ? old('end_published_in_home_at') : $news->end_published_in_home_at, ['width' => '11']) }}
                  </div>

                  {!! Form::bsBtnSave() !!}

                </div>
              </div>
            </div>
          </section>

          <section id="seo">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  <div class="row">
                    {{ Form::bsText('meta_tag_title', old('meta_tag_title') ? old('meta_tag_title') : $news->meta_tag_title, ['width' => 6]) }}
                    {{ Form::bsText('meta_tag_author', old('meta_tag_author') ? old('meta_tag_author') : $news->meta_tag_author, ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('meta_tag_description', old('meta_tag_description') ? old('meta_tag_description') : $news->meta_tag_description, ['width' => 6]) }}
                    {{ Form::bsTags('meta_tag_keywords', old('meta_tag_keywords') ? old('meta_tag_keywords') : $news->meta_tag_keywords, ['width' => 6]) }}
                  </div>

                  {!! Form::bsBtnSave() !!}

                </div>
              </div>
            </div>
          </section>
          <section id="groups">
            <h3>@lang('enteweb.group_access_resource')</h3>

            {{ Form::bsGroupSelect('group[]', null, $news->getGroupsByPermission()) }}

            {!! Form::bsBtnSave() !!}

          </section>
        </div>

        {{ Form::close() }}

      </div>
    </section>

  @endsection

  @push('scripts')

    <script src="{{ asset('/enteweb_public/tinymce/tinymce-init.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/js/cbpFWTabs.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/themes/Backoffice/src/assets/js/includes/post.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>

    <script type="text/javascript">
    $(document).ready(function(){
      jQuery('#published_at').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: '{{ $news->setting->date_format }}',
      });
      jQuery('#end_published_at').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: '{{ $news->setting->date_format }}',
      });
      jQuery('#published_in_home_at').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: '{{ $news->setting->date_format }}',
      });
      jQuery('#end_published_in_home_at').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: '{{ $news->setting->date_format }}',
      });
      @include('enteweb::Includes.saved-alert')
    });

    </script>
  @endpush
