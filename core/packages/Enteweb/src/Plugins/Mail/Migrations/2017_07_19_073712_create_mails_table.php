<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails', function (Blueprint $table) {

            $table->increments('id');

            $table->string('subject');

            $table->string('from');

            $table->string('to');

            $table->text('text');

            $table->boolean('read')->nullable()->default(0);

            $table->boolean('starred')->nullable()->default(0);

            $table->softDeletes();

            $table->timestamps();

            /*
             * Foreign keys
             */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mails');
    }
}
