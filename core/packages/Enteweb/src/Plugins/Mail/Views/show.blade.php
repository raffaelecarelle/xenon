@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')

@endpush

@section('page_title')
  @lang('enteweb.mails')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.mail_detail')</strong>
      </h4>
    </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          @lang('enteweb.breadcrumb_here')
          <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
          <li>@lang('enteweb.mails')</li>
          <li>@lang('enteweb.mail_detail')</li>
        </ol>
      </div>
    </div>
  @endsection

  @section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    @include('mail::Includes.left_bar')
                    <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                        <div class="media m-b-30 p-t-20">
                            <h4 class="font-bold m-t-0">{{ $mail->subject }}</h4>
                            <hr>
                            <div class="media-body"> <span class="media-meta pull-right">{{ Carbon::parse($mail->created_at)->format('d-m-Y H:m:s') }}</span>
                                <small class="text-muted">@lang('enteweb.from') {{ $mail->from }}</small><br />
                                <small class="text-muted">@lang('enteweb.mail_to') {{ $mail->to }}</small>
                            </div>
                        </div>
                        {!! $mail->text !!}
                        <hr>
                        <h4> <i class="fa fa-paperclip m-r-10 m-b-10"></i> @lang('enteweb.attachments') <span>{{ $mail->attachments()->count() }}</span> </h4>
                        <div class="row">
                          @foreach ($mail->attachments()->get() as $attachment)
                            <div class="col-sm-2 col-xs-4">
                              <a href="#"><img class="img-thumbnail img-responsive" alt="attachment" src="{{ asset($attachment->path) }}"> </a>
                            </div>
                          @endforeach
                        </div>
                        <hr>
                        {{-- <div class="b-all p-20">
                            <p class="p-b-20">click here to <a href="#">Reply</a> or <a href="#">Delete</a></p>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

  @endsection

  @push('scripts')

  @endpush
