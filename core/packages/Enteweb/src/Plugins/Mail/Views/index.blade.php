@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
<link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('page_title')
  @lang('enteweb.pages')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.email')
      </h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        @lang('enteweb.breadcrumb_here')
        <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
        <li>@lang('enteweb.email')</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')

  <div class="row">
      <!-- Left sidebar -->
      <div class="col-md-12">
          <div class="white-box">
              <!-- row -->
              <div class="row">
                  @include('mail::Includes.left_bar')
                  <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 mail_listing">
                      <div class="inbox-center">
                          <table class="table table-hover">
                              <thead>
                                  <tr>
                                      <th width="30">
                                          <div class="checkbox m-t-0 m-b-0 ">
                                              <input id="checkbox0" type="checkbox" class="checkbox-toggle" value="check all">
                                              <label for="checkbox0"></label>
                                          </div>
                                      </th>
                                      <th colspan="4">
                                        @if($active == 'inbox')
                                          <div class="btn-group">
                                              <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light m-r-5" data-toggle="dropdown" aria-expanded="false"> @lang('enteweb.filter') <b class="caret"></b> </button>
                                              <ul class="dropdown-menu" role="menu">
                                                  <li><a href="{{ route('enteweb.mail.section', 'read') }}">@lang('enteweb.read')</a></li>
                                                  <li><a href="{{ route('enteweb.mail.section', 'unread') }}">@lang('enteweb.unread')</a></li>
                                              </ul>
                                          </div>
                                          @endif
                                      </th>
                                      <th class="hidden-xs" width="100">
                                          <div class="btn-group pull-right">
                                              {{ $mails->links() }}
                                          </div>
                                      </th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach ($mails as $mail)
                                  <tr @if(!$mail->read)class="unread" @endif>
                                      <td>
                                          <div class="checkbox m-t-0 m-b-0">
                                              <input type="checkbox">
                                              <label for="checkbox0"></label>
                                          </div>
                                      </td>
                                      <td class="hidden-xs"><i class="@if($mail->starred)fa fa-star text-warning @else fa fa-star-o @endif"></i></td>
                                      <td class="hidden-xs">{{ $mail->from }}</td>
                                      <td class="max-texts"> <a href="{{ route('enteweb.mail.show', $mail->id) }}" />{!! $mail->text !!}</td>
                                      </td>
                                      <td class="hidden-xs">@if($mail->attachments()->count() > 0)<i class="fa fa-paperclip"></i>@endif</td>
                                      <td class="text-right"> {{ Carbon::parse($mail->created_at)->format('d-m-Y H:m:s') }} </td>
                                  </tr>
                                  @endforeach
                              </tbody>
                          </table>
                      </div>
                      <div class="row">
                          <div class="col-xs-5 m-t-20">
                              <div class="btn-group pull-right">
                                  {{ $mails->links() }}
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- /.row -->
          </div>
      </div>
  </div>

@endsection

@push('scripts')

  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
  <script>
    $(document).ready(function() {
      @include('enteweb::Includes.saved-alert')
    });
  </script>
@endpush
