<div class="col-lg-2 col-md-3  col-sm-12 col-xs-12 inbox-panel">
    <div>
      <a href="{{ route('enteweb.mail.create') }}" class="btn @if($active == 'create')btn-danger @else btn-custom @endif btn-block waves-effect waves-light">@lang('enteweb.new_mail')</a>
        <div class="list-group mail-list m-t-20">
          <a href="{{ route('enteweb.mail.section', 'inbox') }}" class="list-group-item @if($active == 'inbox')active @endif">@lang('enteweb.inbox') <span class="label label-rouded label-success pull-right">{{ $mailsInboxCount }}</span></a>
          <a href="{{ route('enteweb.mail.section', 'sent') }}" class="list-group-item @if($active == 'sent')active @endif">@lang('enteweb.sent_mail') <span class="label label-rouded label-primary pull-right">{{ $mailsSendCount }}</span></a>
          <a href="{{ route('enteweb.mail.section', 'starred') }}" class="list-group-item @if($active == 'starred')active @endif">@lang('enteweb.starred') <span class="label label-rouded label-warning pull-right">{{ $mailsStarredCount }}</span></a>
          <a href="{{ route('enteweb.mail.section', 'trashed') }}" class="list-group-item @if($active == 'trashed')active @endif">@lang('enteweb.trash') <span class="label label-rouded label-default pull-right">{{ $mailsTrashedCount }}</span></a> </div>
    </div>
</div>
