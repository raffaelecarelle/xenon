@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link rel="stylesheet" href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css') }}" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropzone-master/dist/dropzone.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('page_title')
  @lang('enteweb.mails')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.new_mail')</strong></h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          @lang('enteweb.breadcrumb_here')
          <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
          <li>@lang('enteweb.mails')</li>
          <li>@lang('enteweb.new_mail')</strong></li>
        </ol>
      </div>
    </div>
  @endsection

  @section('content')

    <div class="row">
      <!-- Left sidebar -->
      <div class="col-md-12">
        <div class="white-box">
          <div class="row">
              @include('mail::Includes.left_bar')
              <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                <h3 class="box-title">@lang('enteweb.compose_new_message')</h3>

                {!! Form::model($mail, array('route' => 'enteweb.mail.store', 'id' => 'mainForm')) !!}

                @include('enteweb::Includes.error_display')

                <div class="form-group">
                  <input class="form-control" name="from" value="{{ Auth::user()->email }}" placeholder="@lang('enteweb.from')">
                </div>
                <div class="form-group">
                  <input class="form-control" name="to" type="email" placeholder="@lang('enteweb.to')">
                </div>
                  <div class="form-group">
                    <input class="form-control" name="subject" placeholder="@lang('enteweb.subject')">
                  </div>
                    <div class="form-group">
                      <textarea class="textarea_editor form-control" name="text" rows="15" placeholder="@lang('enteweb.enter_text')"></textarea>
                    </div>

                {{ Form::close() }}

                    <h4><i class="fa fa-link"></i> @lang('enteweb.attachment')</h4>
                    <form action="{{ route('enteweb.mail.attachment') }}" method="post" enctype="multipart/form-data" id="dropzone" class="dropzone">
                      {!! csrf_field() !!}
                      <div class="fallback">
                        <input name="file" type="file" multiple /> </div>
                      </form>
                      <hr>

                      <button type="submit" form="mainForm" class="btn btn-primary"><i class="fa fa-envelope-o"></i> @lang('enteweb.send')</button>

                    </div>
                  </div>
                </div>
              </div>
            </div>

          @endsection

          @push('scripts')
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js') }}"></script>
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js') }}"></script>
            <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
            <script>
            $(document).ready(function() {
              $('.textarea_editor').wysihtml5();
            });
            </script>
          @endpush
