<?php

namespace Enteweb\Plugins\Mail\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
  protected $table = 'attachments';

  protected $fillable = ['title', 'path', 'mail_id'];

  public $rules = [];

  public $tempDir;

  public $attachmentDir;

  public function __construct(){

    $this->tempDir = '/enteweb_public/temp_attachments/';

    $this->attachmentDir = '/enteweb_public/Attachments/';

  }
}
