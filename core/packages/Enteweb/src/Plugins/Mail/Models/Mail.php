<?php

namespace Enteweb\Plugins\Mail\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mail extends Model
{
  use SoftDeletes;

  protected $table = 'mails';

  protected $fillable = [
    'subject',
    'from',
    'to',
    'text',
    'label',
    'read',
    'star',
    'file_id'
  ];

  public $rules = [
    'subject' => 'required',
    'from' => 'required',
    'text' => 'required'
  ];

  public function attachments() {
    return $this->hasMany(Attachment::class);
  }
}
