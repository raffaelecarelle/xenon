<?php

namespace Enteweb\Plugins\Mail\Controllers;

/*
* Laravel classes
*/
use Route;
use Validator;
use App;
use Session;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail as MailFacade;

/*
* Models
*/
use Enteweb\Plugins\Mail\Models\Mail;
use Enteweb\Plugins\Mail\Models\Attachment;

/*
* Mailables
*/
use App\Mail\MailDefaultSender;

class MailController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index($mail_section = 'inbox')
  {

    switch ($mail_section) {
      case 'sent':
      $mails = Mail::where('from', Auth::user()->email)->orderBy('created_at', 'desc')->paginate(15);
      $active = 'sent';
      break;

      case 'read':
      $mails = Mail::where('read', 1)->orderBy('created_at', 'desc')->paginate(15);
      $active = 'inbox';
      break;

      case 'unread':
      $mails = Mail::where('read', 0)->orderBy('created_at', 'desc')->paginate(15);
      $active = 'inbox';
      break;

      case 'starred':
      $mails = Mail::where('starred', 1)->orderBy('created_at', 'desc')->paginate(15);
      $active = 'starred';
      break;

      case 'trashed':
      $mails = Mail::onlyTrashed()->orderBy('created_at', 'desc')->paginate(15);
      $active = 'trashed';
      break;

      case 'create':
      return $this->create();
      break;

      default:
      $mails = Mail::where('from', '!=', Auth::user()->email)->paginate(15);
      $active = 'inbox';
      break;
    }

    $mailsInboxCount = Mail::where('from', '!=', Auth::user()->email)->count();
    $mailsSendCount = Mail::where('from', Auth::user()->email)->count();
    $mailsStarredCount = Mail::where('starred', 1)->count();
    $mailsTrashedCount = Mail::onlyTrashed()->count();
    return view('mail::index',
    compact(
      'mails',
      'active',
      'mailsTrashedCount',
      'mailsInboxCount',
      'mailsSendCount',
      'mailsStarredCount'
    ));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $mail = new Mail;

    $mailsInboxCount = Mail::where('from', '!=', Auth::user()->email)->count();
    $mailsSendCount = Mail::where('from', Auth::user()->email)->count();
    $mailsStarredCount = Mail::where('starred', 1)->count();
    $mailsTrashedCount = Mail::onlyTrashed()->count();

    $previousParameters = app('router')->getRoutes()->match(app('request')->create(\URL::previous()))->parameters();
    $active = 'create';
    return view('mail::create', compact('mail', 'active','mailsInboxCount', 'mailsTrashedCount', 'mailsSendCount', 'mailsStarredCount'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $mail = new Mail;

    $validator = Validator::make($request->all(), array_merge($mail->rules));

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    $data = $request->all();

    $attachment = new Attachment;

    $mail = new Mail;

    $request->request->add(['read' => 1]);

    $mail = $mail->create($request->all());

    $tempDir = public_path() . $attachment->tempDir . Session::getId();

    $mainAttachFolder = $mail->from . '/' . str_slug($request->subject) . '_' . Carbon::now()->timestamp;

    $destinationDir = public_path() . $attachment->attachmentDir . $mainAttachFolder;

    foreach (File::files($tempDir) as $attach) {
      $fileName = explode('/', $attach);
      $fileName = end($fileName);

      $file = new Attachment;

      $file->fill([
        'title' => $fileName,
        'path' => $file->attachmentDir . $mainAttachFolder . '/' . $fileName,
        'mail_id' => $mail->id
      ]);

      $file->save();
    }

    File::copyDirectory($tempDir, $destinationDir);

    File::deleteDirectory($tempDir);

    MailFacade::to($mail->to)->queue(new MailDefaultSender($mail, File::files($destinationDir)));

    Session::flash('saved', trans('enteweb.email_send_with_success'));

    return redirect()->route('enteweb.mail.index');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $previousParameters = app('router')->getRoutes()->match(app('request')->create(\URL::previous()))->parameters();

    switch ($previousParameters['mail_section']) {
      case 'sent':
      $active = 'sent';
      break;

      case 'inbox':
      $active = 'inbox';
      break;

      case 'starred':
      $active = 'starred';
      break;

      case 'trashed':
      $active = 'trashed';
      break;

      default:
      $active = 'inbox';
      break;
    }

    $mail = Mail::findOrFail($id);

    $mailsInboxCount = Mail::where('from', '!=', Auth::user()->email)->count();
    $mailsSendCount = Mail::where('from', Auth::user()->email)->count();
    $mailsStarredCount = Mail::where('starred', 1)->count();
    $mailsTrashedCount = Mail::onlyTrashed()->count();

    return view('mail::show', compact('mail', 'active','mailsInboxCount', 'mailsTrashedCount', 'mailsSendCount', 'mailsStarredCount'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }

  /**
  * Funzione chiamata da dropzone PER OGNI allegato caricato
  * @method uploadAttachment
  * @return void
  */
  public function uploadAttachment() {

    $attachment = new Attachment;

    $destinationPath = public_path() . $attachment->tempDir . Session::getId();

    $attachment = Input::file('file');

    $filename = $attachment->getClientOriginalName();

    Input::file('file')->move($destinationPath, $filename);
  }
}
