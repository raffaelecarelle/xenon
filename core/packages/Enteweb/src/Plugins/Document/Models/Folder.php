<?php

namespace Enteweb\Plugins\Document\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Folder extends Model
{
  use SoftDeletes;
  
  /**
	* Database table.
	*
	* @var string
	*/
	protected $table = 'folders';

	/**
	* The attributes that should be mutated to dates.
	*
	* @var array
	*/
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'name',
		'icon',
		'created_by',
		'updated_by',
		'deleted_by',
		'parent_id'
	];

	/**
	* Validation rules.
	* @var array
	*/
	public $rules = [
		'name' => 'required'
	];
}
