<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('files', function (Blueprint $table)
    {
      $table->increments('id');

      $table->string('name');

      $table->string('extension');

      $table->integer('folder_id')->unsigned()->nullable();

      $table->string('created_by');

      $table->string('updated_by');

      $table->string('deleted_by');

      $table->timestamps();

      $table->softDeletes();

      /*
      * Foreign keys
      */
      $table->foreign('folder_id')->references('id')->on('folders')->onDelete('cascade');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('files');
  }
}
