<?php

namespace Enteweb\Plugins\Template\Models;

/*
 * Laravel classes
 */
use Illuminate\Database\Eloquent\Model;


/*
 * Models
 */

class Layout extends Model
{
  protected $table = 'layouts';

  protected $fillable = [
    'name',
  ];

  public static $rules = [

  ];
}
