<?php

namespace Enteweb\Plugins\Template\Models;

/*
 * Laravel classes
 */
use Illuminate\Database\Eloquent\Model;


/*
 * Models
 */

class Grid extends Model
{
  protected $table = 'grids';

  protected $fillable = [
    'name',
    'pos_x',
    'pos_y',
    'width',
    'height',
    'template_id'
  ];

  public static $rules = [

  ];
}
