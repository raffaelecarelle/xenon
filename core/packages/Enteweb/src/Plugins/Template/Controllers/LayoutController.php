<?php

namespace Enteweb\Plugins\Template\Controllers;

/*
* Laravel classes
*/
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/*
* Models
*/
use Enteweb\Plugins\Template\Models\Layout;
/*
* Extend classes
*/

class LayoutController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $layouts = Layout::all();
    return view('template::Layout.index', compact('layouts'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $layout = new Layout;

    return view('template::Layout.create', compact('layout'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    if (array_key_exists('cancell', $request->all()))
    {
      return redirect()->route('enteweb.photo.index');
    }

    $validator = Validator::make($request->all(), Post::$rules);

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    $post = new Post;

    $post->fill($request->all());
    $post->save();

    /*
    * Associazione lingua col post creato
    */
    $currentLanguage = App::getLocale();

    $language = Language::where('code', $currentLanguage)->first();

    $post->languages()->attach($language->id);

    /*
    * Salvataggio categorie associate
    */
    $post->categories()->attach($request->input('category'));

    /*
    * Creazione record nella tabella IS-A Photo
    */
    $photo = new Photo;

    $request->request->add(['post_id' => $post->id]);

    $routeParams = Route::current()->parameters();

    if(isset($routeParams['photo']))
    {
      $post = Post::find($routeParams['photo']);
    }

    $request->request->add(['img_src' => $request->input('image')]);

    $photo->fill($request->all());
    $photo->save();

    /*
    * Spostamento foto caricate con dropzone da cartella temporanea a cartella definitiva
    * e salvataggio immagine di copertina album
    */
    $destinationDir = public_path() . Photo::$galleryDir . $post->title;

    $tempDir = public_path() . Photo::$tempDir . Session::getId();

    if(File::exists($tempDir))
    {
      foreach (File::files($tempDir) as $file)
      {
        $imgName = explode('/', $file);
        $imgName = end($imgName);

        $photoPost = new Post;

        $photoPost->title = $imgName;
        $photoPost->visible = 1;
        $photoPost->post_type = 'photo';
        $photoPost->parent_id = $post->id;
        $photoPost->slug = str_slug($imgName);
        $photoPost->created_by = Auth::user()->name;

        $photoPost->save();

        // Associo la lingua al post
        $photoPost->languages()->attach($language->id);

        $photoTemp = new Photo;

        $photoTemp->img_src = Photo::$galleryDir . $post->title . '/' . $imgName;
        $photoTemp->img_title = $imgName;
        $photoTemp->img_alt = $imgName;
        $photoTemp->width = $photo->width;
        $photoTemp->height = $photo->height;
        $photoTemp->created_by = Auth::user()->name;
        $photoTemp->post_id = $photoPost->id;

        $photoTemp->save();

        // if(!File::exists($tempDir . '/thumbs'))
        // {
        //   File::makeDirectory($tempDir . '/thumbs');
        //   File::makeDirectory($tempDir . '/thumbs/img_mini');
        //   File::makeDirectory($tempDir . '/thumbs/img_small');
        //   File::makeDirectory($tempDir . '/thumbs/img_preview');
        //   File::makeDirectory($tempDir . '/thumbs/img_medium');
        //   File::makeDirectory($tempDir . '/thumbs/img_large');
        // }
        //
        Image::make($file)->resize(config('image.THUMB_W'), config('image.THUMB_H'))->save(public_path() . Photo::$galleryDir . 'thumbs/' . $imgName);
        // $img = Image::make($file)->resize(config('image.IMG_SMALL_W'), config('image.IMG_SMALL_H'))->save($tempDir . '/thumbs/img_small/' . $title);
        // $img = Image::make($file)->resize(config('image.IMG_PREVIEW_W'), config('image.IMG_PREVIEW_H'))->save($tempDir . '/thumbs/img_preview/' . $title);
        // $img = Image::make($file)->resize(config('image.IMG_MEDIUM_W'), config('image.IMG_MEDIUM_H'))->save($tempDir . '/thumbs/img_medium/' . $title);
        // $img = Image::make($file)->resize(config('image.IMG_LARGE_W'), config('image.IMG_LARGE_H'))->save($tempDir . '/thumbs/img_large/' . $title);
      }
    }

    File::copyDirectory($tempDir, $destinationDir);
    File::deleteDirectory($tempDir);

    // $imgName = explode('/', $request->input('image'));
    // $imgName = end($imgName);

    // $img = Image::make($destinationDir . '/' . $imgName)->resize(config('image.IMG_MINI_W'), config('image.IMG_MINI_H'))->save($destinationDir . '/thumbs/img_mini/' . $imgName);
    // $img = Image::make($destinationDir . '/' . $imgName)->resize(config('image.IMG_SMALL_W'), config('image.IMG_SMALL_H'))->save($destinationDir . '/thumbs/img_small/' . $imgName);
    // $img = Image::make($destinationDir . '/' . $imgName)->resize(config('image.IMG_PREVIEW_W'), config('image.IMG_PREVIEW_H'))->save($destinationDir . '/thumbs/img_preview/' . $imgName);
    // $img = Image::make($destinationDir . '/' . $imgName)->resize(config('image.IMG_MEDIUM_W'), config('image.IMG_MEDIUM_H'))->save($destinationDir . '/thumbs/img_medium/' . $imgName);
    // $img = Image::make($destinationDir . '/' . $imgName)->resize(config('image.IMG_LARGE_W'), config('image.IMG_LARGE_H'))->save($destinationDir . '/thumbs/img_large/' . $imgName);

    Session::flash('saved', trans('enteweb.resource_create_success'));

    if (array_key_exists('save_exit', $request->all()))
    {
      return redirect()->route('enteweb.photo.index');
    }
    else
    {
      return redirect()->route('enteweb.photo.edit', $photo->post()->first()->id);
    }
  }

  /**
  * Display list of images.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $album = Post::find($id);
    $photo = new Photo;
    $categories = $album->categories()->get();
    $childs = $album->childs()->get();

    $photos = $childs->map(function($child, $key)
    {
      return $child->getFullRow();
    });

    return view('photo::Image.index', compact('photos', 'categories', 'album'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $routeParams = Route::current()->parameters();

    $post = Post::find($id);

    $row = $post->getFullRow();

    if($row->parent_id != NULL)
    {
      $album = Post::find($post->parent_id);

      return view('photo::Image.edit')
      ->with('row', $row)
      ->with('album', $album);
    }

    return view('photo::Album.edit')
    ->with('row', $row);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    if (array_key_exists('cancell', $request->all())) {

      return redirect()->route('enteweb.photo.index');

    }

    $post = Post::find($id);

    $photo = $post->photo()->first();

    Post::$rules['slug'] = 'required';

    $validator = Validator::make($request->all(), array_merge(Post::$rules, Photo::$rules));

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    $post->update($request->all());

    /*
    * Salvataggio categorie associate
    */
    $post->categories()->sync($request->input('category'));

    if(NULL !== $request->input('image'))
    {
      $request->request->add(['img_src' => $request->input('image')]);
    }

    $photo->update($request->all());

    /*
    * Spostamento foto caricate con dropzone da cartella temporanea a cartella definitiva
    * e salvataggio immagine di copertina album
    */
    $destinationDir = public_path() . Photo::$galleryDir . $post->title;

    $tempDir = public_path() . Photo::$tempDir . Session::getId();

    if(File::exists($tempDir))
    {
      foreach (File::files($tempDir) as $file)
      {
        /*
        * per ogni foto presente in tempDir creo un record nel DB
        */
        $imgName = explode('/', $file);
        $imgName = end($imgName);

        $photoPost = new Post;

        $photoPost->title = $imgName;
        $photoPost->visible = 1;
        $photoPost->post_type = 'photo';
        $photoPost->parent_id = $post->id;
        $photoPost->slug = str_slug($imgName);
        $photoPost->created_by = Auth::user()->name;

        $photoPost->save();

        // Associo la lingua al post
        $currentLanguage = App::getLocale();
        $language = Language::where('code', $currentLanguage)->first();
        $photoPost->languages()->attach($language->id);

        $photoTemp = new Photo;

        $photoTemp->img_src = Photo::$galleryDir . $post->title . '/' . $imgName;
        $photoTemp->img_title = $imgName;
        $photoTemp->img_alt = $imgName;
        $photoTemp->width = $photo->width;
        $photoTemp->height = $photo->height;
        $photoTemp->created_by = Auth::user()->name;
        $photoTemp->post_id = $photoPost->id;

        $photoTemp->save();

        // $img = Image::make($file)->resize(config('image.IMG_MINI_W'), config('image.IMG_MINI_H'))->save($tempDir . '/thumbs/img_mini/' . $title);
        // $img = Image::make($file)->resize(config('image.IMG_SMALL_W'), config('image.IMG_SMALL_H'))->save($tempDir . '/thumbs/img_small/' . $title);
        // $img = Image::make($file)->resize(config('image.IMG_PREVIEW_W'), config('image.IMG_PREVIEW_H'))->save($tempDir . '/thumbs/img_preview/' . $title);
        // $img = Image::make($file)->resize(config('image.IMG_MEDIUM_W'), config('image.IMG_MEDIUM_H'))->save($tempDir . '/thumbs/img_medium/' . $title);
        // $img = Image::make($file)->resize(config('image.IMG_LARGE_W'), config('image.IMG_LARGE_H'))->save($tempDir . '/thumbs/img_large/' . $title);

      }
    }

    File::copyDirectory($tempDir, $destinationDir);

    if(NULL !== $request->input('image'))
    {
      $imgName = explode('/', $request->input('image'));
      $imgName = end($imgName);

      // $img = Image::make($destinationDir . $imgName)->resize(config('image.IMG_MINI_W'), config('image.IMG_LARGE_H'))->save($destinationDir . '/thumbs/img_mini/' . $imgName);
      // $img = Image::make($destinationDir . $imgName)->resize(config('image.IMG_SMALL_W'), config('image.IMG_SMALL_H'))->save($destinationDir . '/thumbs/img_small/' . $imgName);
      // $img = Image::make($destinationDir . $imgName)->resize(config('image.IMG_PREVIEW_W'), config('image.IMG_PREVIEW_H'))->save($destinationDir . '/thumbs/img_preview/' . $imgName);
      // $img = Image::make($destinationDir . $imgName)->resize(config('image.IMG_MEDIUM_W'), config('image.IMG_MEDIUM_H'))->save($destinationDir . '/thumbs/img_medium/' . $imgName);
      // $img = Image::make($destinationDir . $imgName)->resize(config('image.IMG_LARGE_W'), config('image.IMG_LARGE_H'))->save($destinationDir . '/thumbs/img_large/' . $imgName);
    }

    File::deleteDirectory($tempDir);

    Session::flash('saved', trans('enteweb.resource_edit_success'));

    if (array_key_exists('save_exit', $request->all()))
    {
      return redirect()->route('enteweb.photo.index');
    }
    else
    {
      return redirect()->route('enteweb.photo.edit', $post->id);
    }
  }

  /**
  * Cancella una risorsa ben specifica, identificata dall'id
  * @method destroy
  * @param  Request $request
  * @return redirect
  */
  public function destroy(Request $request)
  {
    Post::findOrFail($request->id)->delete();

    return redirect()->back();
  }

  /**
  * Funzione chiamata da dropzone PER OGNI foto caricata
  * @method multiUpload
  * @return void
  */
  public function multiUpload()
  {
    $photo = new Photo;

    $photos = Input::file('file');

    $destinationPath = public_path() . Photo::$tempDir . Session::getId();

    $filename = $photos->getClientOriginalName();

    Input::file('file')->move($destinationPath, $filename);
  }
}
