@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/jqueryui/jquery-ui.css') }}" rel="stylesheet">
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/gridstack/gridstack.css') }}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
@endpush

@section('page_title')
  @lang('enteweb.layout')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.new_layouts')</strong>
      </h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        @lang('enteweb.breadcrumb_here')
        <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
        <li>@lang('enteweb.layout')</li>
        <li>@lang('enteweb.new_layouts')</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')

  <div class="container-fluid">
    <span style="margin-left:10px"><label for="indexText">Block Name: </label><input type="text" id="indexText"> <button data-bind="click: addNewWidget">Add</button> </span>
    <span style="float:right; margin-right:10px"><button onClick="save()">Save</button></span>
  </div>

  <div class="grid-stack" data-bind="component: {name: 'dashboard-grid', params: $data}"></div>

@endsection

@push('scripts')
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/jqueryui/jquery-ui.min.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.2.0/knockout-min.js"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/gridstack/gridstack.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/gridstack/gridstack.jQueryUI.js') }}"></script>

  <script type="text/javascript">
  ko.components.register('dashboard-grid', {
    viewModel: {
      createViewModel: function (controller, componentInfo) {
        var ViewModel = function (controller, componentInfo) {
          var grid = null;

          this.widgets = controller.widgets;

          this.afterAddWidget = function (items) {
            if (grid == null) {
              grid = $(componentInfo.element).find('.grid-stack').gridstack({
                auto: false,
                float: true
              }).data('gridstack');
            }

            var item = _.find(items, function (i) { return i.nodeType == 1 });
            grid.addWidget(item);
            ko.utils.domNodeDisposal.addDisposeCallback(item, function () {
              grid.removeWidget(item);
            });
          };
        };

        return new ViewModel(controller, componentInfo);
      }
    },
    template: { element: 'gridstack-template' }
  });

  $(function () {
    var Controller = function (widgets) {
      var self = this;

      this.widgets = ko.observableArray(widgets);

      this.addNewWidget = function () {
        var indexVal = $("#indexText").val();
        this.widgets.push({
          id: indexVal,
          x: 0,
          y: 0,
          width: Math.floor(1 + 3 * Math.random()),
          height: Math.floor(1 + 3 * Math.random()),
          auto_position: true
        });
        return false;
      };

      this.deleteWidget = function (item) {
        self.widgets.remove(item);
        return false;
      };
    };

    var widgets = [
      {"x":0,"y":0,"width":12,"height":2, "id": "Header"},
      {"x":0,"y":2,"width":4,"height":5, "id": "Main 1"},
      {"x":8,"y":2,"width":4,"height":5, "id": "Main 2"},
      {"x":4,"y":2,"width":4,"height":5, "id": "Main 3"},
      {"x":0,"y":7,"width":12,"height":2, "id": "Footer"}]


      var controller = new Controller(widgets);
      ko.applyBindings(controller);
    });
    function save(){
      var res = _.map($('.grid-stack .grid-stack-item:visible'), function (el) {
        el = $(el);
        var node = el.data('_gridstack_node');
        return {
            id: el.attr('data-gs-id'),
            x: node.x,
            y: node.y,
            width: node.width,
            height: node.height
        };
      });
    var resStr = JSON.stringify(res);
    alert(resStr);
    console.log(resStr);
    }

    </script>

    <template id="gridstack-template">
      <div class="grid-stack" data-bind="foreach: {data: widgets, afterRender: afterAddWidget}">
        <div class="grid-stack-item" data-bind="attr: {'data-gs-x': $data.x, 'data-gs-y': $data.y, 'data-gs-width': $data.width, 'data-gs-height': $data.height, 'data-gs-auto-position': $data.auto_position, 'data-gs-id': $data.id}">
          <div class="grid-stack-item-content">

            <div style="float:left"></div>
            <div style="float:right"> <i data-bind="click: $root.deleteWidget" class="material-icons shadow" style="cursor: pointer; color:red">clear</i> </div>
            <center><strong><span data-bind="text: id" /></strong></center>

          </div>
        </div></div><!-- NO SPACE BETWEEN THESE CLOSING TAGS -->
      </template>
    @endpush
