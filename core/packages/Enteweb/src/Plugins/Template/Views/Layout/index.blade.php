@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('page_title')
  @lang('enteweb.layouts')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.layouts')
        {!! Form::bsBtnCreate(trans('enteweb.new_layouts'), 'index') !!}
      </h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        @lang('enteweb.breadcrumb_here')
        <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
        <li>@lang('enteweb.layouts')</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="white-box">
        <div class="row el-element-overlay m-b-40">
          @foreach ($layouts as $layout)
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
              <div class="white-box">
                <div class="el-card-item">
                  <div class="el-card-avatar el-overlay-1"> <img src="{{ Form::srcImageByFormat($layout->img_src, 'thumb') }}" title="{{ $layout->img_title }}" alt="{{ $layout->img_alt }}" />
                    <div class="el-overlay scrl-dwn">
                      <ul class="el-info">
                        <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{ route('enteweb.photo.show', $layout->id) }}"><i class="icon-magnifier"></i></a></li>
                        <li><a class="btn default btn-outline" href="{{ route('enteweb.photo.edit', $layout->id) }}"><i class="fa fa-edit"></i></a></li>
                        <li><a class="btn default btn-outline button-delete" data-id="{{ $layout->id }}" data-token="{{ csrf_token() }}" data-url="{{ route('enteweb.photo.destroy', $layout->id) }}"><i class="fa fa-times"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="el-card-content">
                    <h3 class="box-title">{{ $layout->title }}</h3> <small>{{ $layout->subtitle }}</small>
                    <br/> </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  @endsection

  @push('scripts')
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/js/includes/delete-confirm.js') }}"></script>
    <script>
    $(document).ready(function(){
      // Sweet alert saved
      @if(Session::has('saved'))
        swal("Salvato!", "{!! Session::get('saved') !!}", "success")
      @endif
    });
    </script>
  @endpush
