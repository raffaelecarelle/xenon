@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropzone-master/dist/dropzone.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/vendor/laravel-filemanager/css/lfm.css') }}">
@endpush

@section('page_title')
  @lang('enteweb.photo')
@endsection

@section('titlebar')
  
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.edit_album')</strong>
        {!! Form::bsBtnCreate(trans('enteweb.new_album')) !!}
      </h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        @lang('enteweb.breadcrumb_here')
        <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
        <li>@lang('enteweb.photo')</li>
        <li>@lang('enteweb.edit_album')</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')

  <section>
    <div class="sttabs tabs-style-underline">
      <nav>
        <ul>
          <li><a href="#content" class="fa fa-file-text-o"><span> @lang('enteweb.content')</span></a></li>
          <li><a href="#seo" class="fa fa-eye"><span> @lang('enteweb.seo')</span></a></li>
          <li><a href="#dropzone" class="fa fa-picture-o"><span> @lang('enteweb.dropzone')</span></a></li>
          <li><a href="#info" class="fa fa-info"><span> @lang('enteweb.info')</span></a></li>
        </ul>
      </nav>

      {!! Form::bsOpen($row, 'update', null, 'form-material', ['enctype' => 'multipart/form-data']) !!}

      {{ Form::hidden('post_type', 'photo') }}
      {{ Form::hidden('visible', 1) }}
      {{ Form::hidden('updated_by', Auth::user()->name) }}


      <div class="content-wrap">
        <section id="content">
          <div class="row">
            <div class="col-sm-12">
              <div class="white-box p-l-20 p-r-20">

                @include('enteweb::Includes.error_display')

                <div class="row">
                  {{ Form::bsText('title', old('title') ? old('title') : $row->title, ['width' => 6]) }}
                  {{ Form::bsText('subtitle', old('subtitle') ? old('subtitle') : $row->subtitle, ['width' => 6]) }}
                </div>

                <div class="row">
                  {{ Form::bsText('slug', old('slug') ? old('slug') : $row->slug, ['width' => 6]) }}
                  {{ Form::bsText('view', old('view') ? old('view') : $row->view, ['width' => 6]) }}
                </div>

                <div class="row">
                  {{ Form::bsSelect('parent_id', null, $row->allRowsWithoutMyself(), ['width' => '6']) }}
                  {{ Form::bsNumber('order', old('order') ? old('order') : $row->order, ['width' => '6']) }}
                </div>

                <div class="row">
                  {{ Form::bsMultiSelect('category[]', $row->categories()->get()->pluck('id', 'title'), $row->getCategories(), ['width' => '12'], 'Associa le categorie') }}
                </div>

                <div class="row">
                  {{ Form::bsNumber('width', old('width') ? old('width') : $row->width , ['width' => '6']) }}
                  {{ Form::bsNumber('height', old('height') ? old('height') : $row->height , ['width' => '6']) }}
                </div>

                <div class="row">
                  {{ Form::bsText('img_title', old('img_title') ? old('img_title') : $row->img_title , ['width' => '6']) }}
                  {{ Form::bsText('img_alt', old('img_alt')  ? old('img_alt') : $row->img_alt, ['width' => '6']) }}
                </div>

                <div class="row">
                  <div class="col-md-12">
                    {{ Form::label(trans('enteweb.cover_image'), trans('enteweb.cover_image')) }}
                  </div>
                  {{ Form::bsImageSelector('image', isset($row->img_src) ? $row->img_src : '') }}
                </div>

                <div class="row">
                  {!! Form::bsBtnSave() !!}
                </div>

              </div>
            </div>
          </div>
        </section>
        <section id="seo">
          <div class="row">
            <div class="col-sm-12">
              <div class="white-box p-l-20 p-r-20">

                <div class="row">
                  {{ Form::bsText('meta_tag_title', old('meta_tag_title')  ? old('meta_tag_title') : $row->meta_tag_title, ['width' => 6]) }}
                  {{ Form::bsText('meta_tag_author', old('meta_tag_author')  ? old('meta_tag_author') : $row->meta_tag_author, ['width' => 6]) }}
                </div>

                <div class="row">
                  {{ Form::bsText('meta_tag_description', old('meta_tag_description')  ? old('meta_tag_description') : $row->meta_tag_description, ['width' => 6]) }}
                  {{ Form::bsTags('meta_tag_keywords', old('meta_tag_keywords')  ? old('meta_tag_keywords') : $row->meta_tag_keywords, ['width' => 6]) }}
                </div>

                {!! Form::bsBtnSave() !!}

              </div>
            </div>
          </div>
        </section>

        {{ Form::close() }}

        <section id="dropzone">

          {{ Form::bsDropZone(route('enteweb.photo.multiupload')) }}

        </section>

        <section id="info">
          <div class="row">
            <div class="col-lg-12">
              <div class="white-box">
                <h3 class="box-title m-b-0">@lang('enteweb.info_page')</h3>
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>@lang('enteweb.title')</th>
                        <th>@lang('enteweb.created_by')</th>
                        <th>@lang('enteweb.created_at')</th>
                        <th>@lang('enteweb.updated_by')</th>
                        <th>@lang('enteweb.updated_at')</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{ $row->title }}</td>
                        <td>{{ $row->created_by }}</td>
                        <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $row->created_at }} </span> </td>
                        @if($row->updated_by)
                          <td>{{ $row->updated_by }}</td>
                          <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{ $row->updated_at }} </span> </td>
                        @else
                          <td> --- </td>
                          <td><span class="text-muted"><i class="fa fa-clock-o"></i> --- </span> </td>
                        @endif
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- /tabs -->
  </section>

@endsection

@push('scripts')
  <script src="{{ asset('/themes/Backoffice/src/assets/js/cbpFWTabs.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
  <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/js/includes/photo.js') }}"></script>

  <script type="text/javascript">
    jQuery(document).ready(function() {
      @include('enteweb::Includes.saved-alert')
    });
  </script>
@endpush
