@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link rel="stylesheet" type="text/css" href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/gallery/css/animated-masonry-gallery.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/fancybox/ekko-lightbox.min.css') }}" />
@endpush

@section('page_title')
  @lang('enteweb.photo')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.photo')
        {!! Form::bsBtnCreate(trans('enteweb.new_photo'), 'show', 'createintoalbum', $album->id) !!}
      </h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        @lang('enteweb.breadcrumb_here')
        <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
        <li>@lang('enteweb.photo')</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="white-box">
        <div id="gallery">
          <div id="gallery-header">
            <div id="gallery-header-center">
              <div id="gallery-header-center-left">
                <div class="gallery-header-center-right-links" id="filter-all">@lang('enteweb.all')</div>
                @foreach ($categories as $category)
                  <div class="gallery-header-center-right-links" id="filter-{{ $category->slug }}">{{ $category->title }}</div>
                @endforeach
              </div>
            </div>
          </div>
          <div id="gallery-content">
            <div id="gallery-content-center">
              @foreach ($photos as $photo)
                <a href="{{ route('enteweb.photo.edit', $photo->id) }}" data-toggle="lightbox" data-gallery="multiimages" data-title="{{ $photo->img_title }}">
                  <img src="{{ Form::srcImageByFormat($photo->img_src, 'thumb') }}" alt="{{ $photo->img_alt }}"
                  class="all @foreach($photo->categories()->get() as $category) {{ $category->slug }} @endforeach" />
                  </a>
                @endforeach
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  @endsection

  @push('scripts')
    <script type="text/javascript" src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/gallery/js/jquery.isotope.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/fancybox/ekko-lightbox.min.js') }}"></script>
    @include('enteweb::Includes.image-filter')
    <script type="text/javascript">
    $( function() {
      $('#filter-all').click();

      setTimeout(function() {
        $('#filter-all').trigger('click');
      }, 500);
    });
    </script>
  @endpush
