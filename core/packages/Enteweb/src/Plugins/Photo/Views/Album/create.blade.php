@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropzone-master/dist/dropzone.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/vendor/laravel-filemanager/css/lfm.css') }}">
@endpush

@section('page_title')
  @lang('enteweb.photo')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.new_album')</strong></h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          @lang('enteweb.breadcrumb_here')
          <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
          <li>@lang('enteweb.photo')</li>
          <li>@lang('enteweb.new_album')</strong></li>
        </ol>
      </div>
    </div>
  @endsection

  @section('content')

    <section>
      <div class="sttabs tabs-style-underline">
        <nav>
          <ul>
            <li><a href="#content" class="fa fa-file-text-o"><span> @lang('enteweb.content')</span></a></li>
            <li><a href="#seo" class="fa fa-eye"><span> @lang('enteweb.seo')</span></a></li>
            <li><a href="#dropzone" class="fa fa-picture-o"><span> @lang('enteweb.dropzone')</span></a></li>
          </ul>
        </nav>

        {!! Form::bsOpen($row, 'store', null, 'form-material', ['enctype' => 'multipart/form-data']) !!}

        {{ Form::hidden('post_type', 'photo') }}
        {{ Form::hidden('visible', '1') }}
        {{ Form::hidden('created_by', Auth::user()->name) }}

        <div class="content-wrap">
          <section id="content">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  @include('enteweb::Includes.error_display')

                  <div class="row">
                    {{ Form::bsText('title', old('title'), ['width' => 6]) }}
                    {{ Form::bsText('subtitle', old('subtitle'), ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('slug', old('slug'), ['width' => 6]) }}
                    {{ Form::bsText('view', old('view'), ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsSelect('parent_id', old('parent_id'), $row->allRowsWithoutMyself('photo'), ['width' => '6']) }}
                    {{ Form::bsNumber('order', old('order'), ['width' => '6']) }}
                  </div>

                  <div class="row">
                    {{ Form::bsNumber('width', old('width'), ['width' => '6']) }}
                    {{ Form::bsNumber('height', old('height'), ['width' => '6']) }}
                  </div>

                  <div class="row">
                    {{ Form::bsMultiSelect('category[]', old('category_id'), $row->getCategories(), ['width' => '12'], 'Associa le categorie') }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('img_title', old('img_title'), ['width' => '6']) }}
                    {{ Form::bsText('img_alt', old('img_alt'), ['width' => '6']) }}
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      {{ Form::label(trans('enteweb.cover_image'), trans('enteweb.cover_image')) }}
                    </div>
                    {{ Form::bsImageSelector('image') }}
                  </div>

                  <div class="row">
                    {!! Form::bsBtnSave() !!}
                  </div>

                </div>
              </div>
            </div>
          </section>
          <section id="seo">
            <div class="row">
              <div class="col-sm-12">
                <div class="white-box p-l-20 p-r-20">

                  <div class="row">
                    {{ Form::bsText('meta_tag_title', null, ['width' => 6]) }}
                    {{ Form::bsText('meta_tag_author', null, ['width' => 6]) }}
                  </div>

                  <div class="row">
                    {{ Form::bsText('meta_tag_description', null, ['width' => 6]) }}
                    {{ Form::bsTags('meta_tag_keywords', null, ['width' => 6]) }}
                  </div>

                  {!! Form::bsBtnSave() !!}

                </div>
              </div>
            </div>
          </section>

          {{ Form::close() }}

          <section id="dropzone">

            {{ Form::bsDropZone(route('enteweb.photo.multiupload')) }}

          </section>
        </div>
      </div>
      <!-- /tabs -->
    </section>

  @endsection

  @push('scripts')
    <script src="{{ asset('/themes/Backoffice/src/assets/js/cbpFWTabs.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script src="{{ asset('/themes/Backoffice/src/assets/js/includes/photo.js') }}"></script>
  @endpush
