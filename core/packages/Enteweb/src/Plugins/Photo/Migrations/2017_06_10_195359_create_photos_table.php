<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            $table->string('img_src');

            $table->string('img_title')->nullable();

            $table->string('img_alt')->nullable();

            $table->integer('height')->nullable();

            $table->integer('width')->nullable();

            $table->integer('post_id')->unsigned()->nullable();

            $table->string('created_by')->nullable();

            $table->string('updated_by')->nullable();

            $table->timestamps();

            /**
            * Foreignkeys section
            */
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
