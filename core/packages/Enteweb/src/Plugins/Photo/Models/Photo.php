<?php

namespace Enteweb\Plugins\Photo\Models;

/*
 * Laravel classes
 */
use Illuminate\Database\Eloquent\Model;


/*
 * Models
 */
use Enteweb\Plugins\Post\Models\Post;

class Photo extends Model
{
  protected $table = 'photos';

  protected $fillable = [
    'post_id',
    'img_src',
    'img_alt',
    'img_title',
    'width',
    'height'
  ];

  public static $tempDir;

  public static $galleryDir;

  public static $rules = [
    'img_src' => 'require'
  ];

  public function __construct()
  {
    self::$tempDir = '/enteweb_public/file-manager/temp_files/';
    self::$galleryDir = '/enteweb_public/file-manager/files/shares/';
  }

  public function post()
  {
    return $this->belongsTo(Post::class);
  }
}
