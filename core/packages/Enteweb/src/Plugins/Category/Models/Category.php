<?php

namespace Enteweb\Plugins\Category\Models;

/*
 * Laravel classes
 */
use Illuminate\Database\Eloquent\Model;

/*
 * Models
 */
use Enteweb\Plugins\Language\Models\Language;

/**
* Scopes
*/
use Extend\Scopes\LanguagesScope;

class Category extends Model {
    protected $table = 'categories';

    protected $fillable = [
      'title',
      'subtitle',
      'slug',
      'parent_id',
      'created_by',
      'updated_by'
    ];

    public $rules = [
      'title' => 'required',
      'slug' => 'required|unique:categories'
    ];

    /**
    * The "booting" method of the model.
    *
    * @return void
    */
    protected static function boot()
    {
      parent::boot();

      static::addGlobalScope(new LanguagesScope);
    }

    /**
    * Parent
    *
    * @return  \Illuminate\Support\Collection;
    */
    public function parent(){

      return $this->belongsTo(Category::class, 'id', 'parent_id');

    }

    /**
    * Child
    * @param
    * @return  \Illuminate\Support\Collection;
    */
    public function childs(){

      return $this->hasMany(Category::class, 'parent_id', 'id');

    }

    /**
  	* Get all of the posts that are assigned this category..
  	*
  	* @return  \Illuminate\Support\Collection;
  	*/
    public function posts(){

      return $this->morphToMany(Post::class, 'associable');

    }

    /**
  	* Get all of the languages that are assigned this category..
  	*
  	* @return  \Illuminate\Support\Collection;
  	*/
    public function languages(){

      return $this->belongsToMany(Language::class);

    }

    /**
    * Funzione che trova tutte le pagine a parte l'istanza chiamante
    *  per la scelta del padre.
    *  @return \Illuminate\Support\Collection;
    */
    public function allCategoriesWithoutMyself() {

      $categories = $this->where('id', '!=', $this->id)
      ->pluck('title', 'id')
      ->prepend('------', 'NULL');

      return $categories;

    }
}
