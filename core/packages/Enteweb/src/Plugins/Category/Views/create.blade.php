@extends('admin_theme::layouts.app')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('page_title')
  @lang('enteweb.categories')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.new_category')</strong></h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          @lang('enteweb.breadcrumb_here')
          <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
          <li>@lang('enteweb.categories')</li>
          <li>@lang('enteweb.new_category')</strong></li>
        </ol>
      </div>
    </div>
  @endsection

  @section('content')

    @include('admin_theme::components.components')

    <section>
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box p-l-20 p-r-20">
            <div class="row">
              <div class="col-md-12">

                {!! Form::bsOpen($row, 'store', null, 'form-material') !!}

                {{ Form::hidden('created_by', Auth::user()->name) }}
                @include('enteweb::Includes.error_display')
                
                <div class="row">
                  {{ Form::bsText('title', old('title'), ['width' => 6]) }}
                  {{ Form::bsText('subtitle', old('subtitle'), ['width' => 6]) }}
                </div>

                <div class="row">
                  {{ Form::bsSelect('parent_id', null, $row->allCategoriesWithoutMyself(), ['width' => '6']) }}
                  {{ Form::bsText('slug', old('slug'), ['width' => '6']) }}
                </div>

                <div class="row">
                  {!! Form::bsBtnSave() !!}
                </div>

                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  @endsection

  @push('scripts')
    <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script>
    jQuery(document).ready(function() {
      // For select 2
      $(".select2").select2();
    });
    </script>
  @endpush
