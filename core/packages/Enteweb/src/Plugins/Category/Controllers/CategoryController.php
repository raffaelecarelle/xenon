<?php

namespace Enteweb\Plugins\Category\Controllers;

/*
 * Laravel classes
 */
use App;
use Validator;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/*
 * Models
 */
use Enteweb\Plugins\Category\Models\Category;
use Enteweb\Plugins\Language\Models\Language;

/*
* Extend classes
*/
use Extend\Traits\CrudTrait;

class CategoryController extends Controller {
    use CrudTrait;

    public function index(){

      $categories = Category::all();

      $view = $this->getView();

      return view($view)->with('categories', $categories);
    }

    /**
    * Salva una nuova categoria
    * @method store
    * @param  Request $request
    * @return void
    */
    public function store(Request $request){

      if (array_key_exists('cancell', $request->all())) {

        return redirect()->route('enteweb.category.index');

      }

      $category = new Category;

      $validator = Validator::make($request->all(), $category->rules);

      if ($validator->fails()) {
        return redirect()
        ->back()
        ->withErrors($validator)
        ->withInput();
      }

      $category->fill($request->all());
      $category->save();

      /*
      * Associazione lingua con la categoria creata
      */
      $currentLanguage = App::getLocale();
      $language = Language::where('code', $currentLanguage)->first();
      $category->languages()->attach($language->id);

      Session::flash('saved', trans('enteweb.resource_create_success'));

      if (array_key_exists('save_exit', $request->all())) {

        return redirect()->route('enteweb.category.index');

      }else{

        return redirect()->route('enteweb.category.edit', $category->id);

      }
    }

    /**
    * Aggiorna uno specifica categoria
    * @method update
    * @param  Request $request, $id
    * @return void
    */
    public function update(Request $request, $id) {

      if (array_key_exists('cancell', $request->all())) {

        return redirect()->route('enteweb.category.index');

      }

      $category = Category::find($id);

      $category->rules['slug'] = 'required';

      $validator = Validator::make($request->all(), $category->rules);

      if ($validator->fails()) {
        return redirect()
        ->back()
        ->withErrors($validator)
        ->withInput();
      }

      $category->update($request->all());

      Session::flash('saved', trans('enteweb.resource_edit_success'));

      if (array_key_exists('save_exit', $request->all())) {

        return redirect()->route('enteweb.category.index');

      }else{

        return redirect()->route('enteweb.category.edit', $category->id);

      }
    }
}
