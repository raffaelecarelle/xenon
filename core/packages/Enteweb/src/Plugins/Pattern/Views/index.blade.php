@extends('admin_theme::layouts.app')

@section('page_title')
  Modelli
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">Modelli</h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          @lang('enteweb.breadcrumb_here')
          <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
          <li>Modelli</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>
@endsection

@section('content')

@endsection
