@extends('admin_theme::layouts.app')

@include('admin_theme::components.components')

@push('styles')
<link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<style>
.dataTables_filter { display: none }
.dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
    color: #fff !important;
    border: 1px solid #03A9F3;
    background-color: #03A9F3;
}
</style>
@endpush

@section('page_title')
  @lang('enteweb.pages')
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">@lang('enteweb.pages')
        {!! Form::bsBtnCreate(trans('enteweb.new_page'), 'index') !!}
      </h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
        @lang('enteweb.breadcrumb_here')
        <li><a href="{{ route('enteweb.dashboard') }}">Home</a></li>
        <li>@lang('enteweb.pages')</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')

  <div class="row">
    <div class="col-sm-12">
      <div class="white-box">
        <div class="table-responsive">
          <table id="table" class="table table-striped">
            <thead>
              <tr>
                <th class="text-center">@lang('enteweb.id')</th>
                <th class="text-center">@lang('enteweb.title')</th>
                <th class="text-center">@lang('enteweb.subtitle')</th>
                <th class="text-center">@lang('enteweb.visible')</th>
                <th class="text-center">@lang('enteweb.actions')</th>
              </tr>
            </thead>
            <tbody>

              @foreach($posts as $post)
                <tr>
                  <td class="text-center">{{ $post->id }}</td>
                  <td>{{ $post->title }}</td>
                  <td>{{ $post->subtitle }}</td>
                  <td class="text-center">{!! Form::status($post->visible, 'Pubblicato') !!}</td>
                  <td class="text-center">
                    {!! Form::bsBtnActions($post, trans('enteweb.edit'), trans('enteweb.delete'),
                          route('enteweb.post.edit', $post->id), route('enteweb.post.destroy', $post->id)) !!}
                  </td>
                </tr>
              @endforeach

            </tbody>
            <tfoot>
              <tr>
                <th>@lang('enteweb.id')</th>
                <th>@lang('enteweb.title')</th>
                <th>@lang('enteweb.subtitle')</th>
                <th>@lang('enteweb.visible')</th>
                <th>@lang('enteweb.actions')</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('scripts')
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>

  <script src="{{ asset('/themes/Backoffice/src/assets/js/includes/index.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/js/includes/delete-confirm.js') }}"></script>
  <script>
    $(document).ready(function() {
      @include('enteweb::Includes.saved-alert')
    });
  </script>
@endpush
