<?php

namespace Enteweb\Plugins\Post\Models;

/*
* Laravel classes
*/
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Auth;
use Laravel\Scout\Searchable;

/**
* Scopes
*/
use Extend\Scopes\LanguagesScope;
use Extend\Scopes\VisibleScope;

/*
* Models
*/
use Enteweb\Plugins\Link\Models\Link;
use Enteweb\Plugins\News\Models\News;
use Enteweb\Plugins\Photo\Models\Photo;
use Enteweb\Plugins\Form\Models\Form;
use Enteweb\Plugins\Category\Models\Category;
use Enteweb\Plugins\Language\Models\language;
use Enteweb\Plugins\Setting\Models\Setting;
use LiveUsers\Models\Group;
use LiveUsers\Models\Permission;

class Post extends Model
{
  use Searchable;

  protected $table = 'posts';

  /**
  * The attributes that should be mutated to dates.
  *
  * @var array
  */
  protected $dates = ['created_at', 'updated_at'];

  protected $fillable = [
    'title',
    'subtitle',
    'visible',
    'created_by',
    'updated_by',
    'slug',
    'view',
    'order',
    'text',
    'parent_id',
    "meta_tag_title",
    "meta_tag_author",
    "meta_tag_description",
    "meta_tag_keywords",
    'post_type'
  ];

  public $setting;

  public function __construct(){

    $this->setting = Setting::active()->first();

  }

  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();

    // Aggiungo i global scope che mi servono a questo Model
    static::addGlobalScope(new LanguagesScope);
  }

  /*
  * Regole di validazione
  */
  public static $rules = [
    'title' => 'required',
    'post_type' => 'required',
    'slug' => 'unique:posts|required'
  ];

  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  ///////               RELATIONSHIPS                               ////////
  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////

  /**
  * Parent
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function parent(){

    return $this->belongsTo(Post::class, 'id', 'parent_id');

  }

  /**
  * Child
  * @param post_type
  * @return  \Illuminate\Support\Collection;
  */
  public function childs(){

    return $this->hasMany(Post::class, 'parent_id', 'id');

  }

  /**
  * Category
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function categories(){

    return $this->morphedByMany(Category::class, 'associable');

  }

  /**
  * Get all the link that are assigned this post.
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function links(){

    return $this->morphedByMany(Post::class, 'associable');

  }

  /**
  * Get the link that are assigned this post (type = link)..
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function link(){

    return $this->hasOne(Link::class);

  }

  /**
  * Get the form that are assigned this post (type = form)..
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function form(){

    return $this->hasOne(Form::class);

  }

  /**
  * Get the news that are assigned this post (type = news)..
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function news(){

    return $this->hasOne(News::class);

  }

  /**
  * Get the photo that are assigned this post (type = photo)..
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function photo(){

    return $this->hasOne(Photo::class);

  }

  /**
  * Group
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function groups(){

    return $this->morphToMany(Group::class, 'groupable');

  }

  /**
  * Languages
  *
  * @return  \Illuminate\Support\Collection;
  */
  public function languages(){

    return $this->morphedByMany(Language::class, 'associable');

  }

  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  ///////                     SCOPES                                ////////
  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////

  public function scopeNotDeleted($query) {
    return $query->whereDeletedAt(NULL);
  }

  /**
  * Scope che permette di specificare il tipo di post cercato.
  */
  public function scopeType($query, $type) {
    return $query->wherePost_type($type);
  }

  /**
  * Scope che permette di estrarre gli album dalla tabella post.
  */
  public function scopeAlbums($query) {
    return $query->type('photo')->where('parent_id', NULL);
  }

  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  ///////              HELPER FUNCTIONS                             ////////
  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////

  /**
  * Funzione che trova tutte le row a parte l'istanza chiamante
  *  per la scelta del padre.
  *  @return \Illuminate\Support\Collection;
  */
  public function allRowsWithoutMyself($post_type = NULL) {

    if($post_type == NULL) {
      $post_type = $this->post_type;
    }

    $rows = $this->where('id', '!=', $this->id)
    ->where('post_type', $post_type)
    ->pluck('title', 'id')
    ->prepend('------', '');

    return $rows;

  }

  /**
  * Trovo tutti i post di un certo "tipo" (post_type) e tutte le row nella tabella specializzata
  *  per poi unirle in una row sola.
  *  @return \Illuminate\Support\Collection;
  */

  public static function getFullRows($post_type) {

    $posts = self::type($post_type)->notDeleted()->get();

    $completeRows = $posts->map(function($post, $key) use ($post_type){

      $resource = $post->$post_type()->first();

      $full = array_merge($resource->toArray(), $post->toArray());

      return (object) $full;

    });

    return $completeRows;
  }

  /**
  * Data l'instanza chiamante restituisce la row completa "post + post_type"
  *  @return \Illuminate\Support\Collection;
  */
  public function getFullRow() {

    $post_type = $this->post_type;

    $resource = $this->$post_type()->first();

    $fullRow = array_merge($resource->toArray(), $this->getAttributes());

    foreach ($fullRow as $key => $value) {

      $this->$key = $value;

    }

    return $this;
  }

  /**
  * Restituisce tutte le categorie
  * @method getCategories
  * @return  \Illuminate\Support\Collection;
  */
  public function getCategories() {
    $category = Category::all();

    return $category->pluck('title', 'id');
  }

  /**
  * Restituisce tutte i gruppi con lo stesso livello (o livello inferiore) di permessi.
  * @method getGroupsByPermission
  * @return  \Illuminate\Support\Collection;
  */
  public function getGroupsByPermission() {

    //$userPermission = Auth::user()->permissions()->first();

    //$permissions = Permission::where('type', '<=',$userPermission->type)->get();
    return [];
    // $usersByPermission = $permissions->map(function($permission, $key){
    //   return $permission->users()->get();
    // });
    // dd($usersByPermission);
    // $groupByOwner = $usersByPermission->map(function($user, $key){
    //   return $user->groups()->get();
    // });
    // dd($groupByOwner);
    // return $groupsByPermission->pluck('name', 'id');
  }
}
