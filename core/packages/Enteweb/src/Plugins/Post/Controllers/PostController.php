<?php

namespace Enteweb\Plugins\Post\Controllers;

/*
* Laravel classes
*/
use App;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

/*
* Extend classes
*/
use Extend\Traits\CrudTrait;

/*
* Models
*/
use Enteweb\Plugins\Post\Models\Post;
use Enteweb\Plugins\Language\Models\Language;

class PostController extends Controller {

  use CrudTrait;

  /**
  * Salva un nuovo post
  * @method store
  * @param  Request $request
  * @return void
  */
  public function store(Request $request) {

    if (array_key_exists('cancell', $request->all())) {

      return redirect()->route('enteweb.post.index');

    }

    $post = new Post;

    $validator = Validator::make($request->all(), Post::$rules);

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    $post->fill($request->all());
    $post->save();

    /*
    * Associazione lingua col post creato
    */
    $currentLanguage = App::getLocale();
    $language = Language::where('code', $currentLanguage)->first();
    $post->languages()->attach($language->id);

    /*
    * Salvataggio categorie associate
    */
    $post->categories()->attach($request->input('category'));


    /*
    * Salvataggio link associati
    */
    $post->links()->attach($request->input('link'));


    /*
    * Salvataggio gruppi associati
    */
    $groups_ids = array_filter(explode(',', $request->input('group')));
    $post->groups()->attach($groups_ids);

    Session::flash('saved', trans('enteweb.resource_create_success'));

    if (array_key_exists('save_exit', $request->all())) {

      return redirect()->route('enteweb.post.index');

    }else{

      return redirect()->route('enteweb.post.edit', $post->id);

    }
  }

  /**
  * Aggiorna uno specifico post
  * @method update
  * @param  Request $request, $id
  * @return void
  */
  public function update(Request $request, $id) {

    if (array_key_exists('cancell', $request->all())) {

      return redirect()->route('enteweb.post.index');

    }

    $post = Post::find($id);
    
    Post::$rules['slug'] = 'required';

    $validator = Validator::make($request->all(), Post::$rules);

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    $post->update($request->all());

    /*
    * Update categorie associate
    */
    $post->categories()->sync($request->input('category'));

    /*
    * Update link associati
    */

    //$links_ids = array_filter(explode(',', $request->input('link')));
    $post->links()->sync($request->input('link'));


    /*
    * Update gruppi associati
    */
    $groups_ids = array_filter(explode(',', $request->input('group')));
    $post->groups()->sync($groups_ids);

    Session::flash('saved', trans('enteweb.resource_edit_success'));

    if (array_key_exists('save_exit', $request->all())) {

      return redirect()->route('enteweb.post.index');

    }else{

      return redirect()->route('enteweb.post.edit', $post->id);

    }
  }
}
