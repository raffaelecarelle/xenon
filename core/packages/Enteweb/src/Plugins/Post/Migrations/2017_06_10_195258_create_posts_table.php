<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {

            $table->increments('id');

            $table->string('title');

            $table->string('subtitle')->nullable();

            $table->boolean('visible');

            $table->text('text')->nullable();

            $table->string('view')->nullable();

            $table->integer('order')->nullable()->default(0);

            $table->string('post_type');

            $table->integer('parent_id')->unsigned()->nullable();

            $table->string('meta_tag_title')->nullable();

            $table->string('meta_tag_author')->nullable();

            $table->string('meta_tag_description')->nullable();

            $table->string('meta_tag_keywords')->nullable();

            $table->string('slug');

            $table->string('created_by')->nullable();

            $table->string('updated_by')->nullable();

            //$table->softDeletes();

            $table->timestamps();

            /**
            * Foreignkeys section
            */
            $table->foreign('parent_id')->references('id')->on('posts')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
