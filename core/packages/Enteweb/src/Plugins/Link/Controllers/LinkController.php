<?php

namespace Enteweb\Plugins\Link\Controllers;

/*
* Laravel classes
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App;

/*
* Models
*/
use Enteweb\Plugins\Post\Models\Post;
use Enteweb\Plugins\Link\Models\Link;
use Enteweb\Plugins\Language\Models\Language;

/*
 * Extedn classes
 */



class LinkController extends Controller {
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index() {

    $links = Post::getFullRows('link');

    return view('link::index', compact('links'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create() {
    $link = new Post;

    return view('link::create', compact('link'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request) {

    if (array_key_exists('cancell', $request->all())) {

      return redirect()->route('enteweb.link.index');

    }

    $post = new Post;

    $link = new Link;

    $validator = Validator::make($request->all(), array_merge($post->rules, $link->rules));

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    if($request->input('parent_id') == 'NULL'){
      $request->merge(['parent_id' => NULL]);
    }

    $post->fill($request->all());
    $post->save();

    $request->request->add(['post_id' => $post->id]);

    $link->create($request->all());

    /*
    * Associazione lingua col post creato
    */
    $currentLanguage = App::getLocale();
    $language = Language::where('code', $currentLanguage)->first();
    $post->languages()->attach($language->id);

    /*
    * Salvataggio categorie associate
    */
    $post->categories()->attach($request->input('category'));


    /*
    * Salvataggio gruppi associati
    */
    $post->groups()->attach($request->input('group'));

    Session::flash('saved', trans('enteweb.resource_create_success'));

    if (array_key_exists('save_exit', $request->all())) {

      return redirect()->route('enteweb.link.index');

    }else{

      return redirect()->route('enteweb.link.edit', $post->id);

    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id) {

    $link = Post::findOrFail($id)->getFullRow();

    return view('link::edit', compact('link'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id) {

    if (array_key_exists('cancell', $request->all())) {

      return redirect()->route('enteweb.link.index');

    }

    $post = Post::findOrFail($id);

    $link = $post->link()->first();

    $post->rules['slug'] = 'required';

    $validator = Validator::make($request->all(), array_merge($post->rules, $link->rules));

    if ($validator->fails()) {
      return redirect()
      ->back()
      ->withErrors($validator)
      ->withInput();
    }

    if($request->input('parent_id') == 'NULL'){
      $request->merge(['parent_id' => NULL]);
    }

    $post->update($request->all());

    $link->update($request->all());

    /*
    * Salvataggio categorie associate
    */
    $post->categories()->sync($request->input('category'));


    /*
    * Salvataggio gruppi associati
    */
    $post->groups()->sync($request->input('group'));

    Session::flash('saved', trans('enteweb.resource_edit_success'));

    if (array_key_exists('save_exit', $request->all())) {

      return redirect()->route('enteweb.link.index');

    }else{

      return redirect()->route('enteweb.link.edit', $post->id);

    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request) {

    $link = Post::findOrFail($request->id)->delete();

    return redirect()->back();

  }
}
