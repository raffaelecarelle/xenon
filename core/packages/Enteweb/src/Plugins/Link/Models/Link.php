<?php

namespace Enteweb\Plugins\Link\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * Models
 */
use Enteweb\Models\Post;

class Link extends Model
{
    protected $table = 'links';

    protected $fillable = ['link','post_id'];

    public $rules = [
      'link' => 'required'
    ];

    /**
  	* Post
  	*
  	* @return  \Illuminate\Support\Collection;
  	*/
    public function Post(){
      return $this->hasMany(Post::class);
    }
}
