<?php

namespace Enteweb;

use File;
use View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Storage;
use Auth;
/*
* Models
*/
use LiveUsers\Models\Area;

/*
* Libraries
*/
use \Extend\Libraries\AdminTheme;

class EntewebServiceProvider extends ServiceProvider {
  /**
  * Bootstrap the application services.
  *
  * @return void
  */
  public function boot() {

    $path_to_plugins = __DIR__ . '/Plugins/';

    if(Schema::hasTable('areas')) {
      //compose all the views....
      view()->composer('*', function ($view) {
        $areas = Area::where('parent_id', NULL)->enable()->with('permissions')
        ->whereHas('permissions', function($query){
          if(Auth::check()){
            $query->untilUserType();
          }
          else{
            $query->where('type', 5);
          }
        })->get();

        // Inietto dentro ad ogni area i suoi figli
        $areas->map(function($area, $key) {
          $area->childs = $area->childs()->enable()->with('permissions')
          ->whereHas('permissions', function($query){
            if(Auth::check()){
              $query->untilUserType();
            }
            else{
              $query->where('type', 5);
            }
          })->get();
        });

        $adminTheme = new AdminTheme;

        $adminTheme->setTheme(config('enteweb.admin_theme'));

        /*
        * Condivide con tutte le view esistenti la variabile $areas
        */
        $view->with('areasNavBar', $areas )->with('adminTheme', $adminTheme);
      });

    } else {

      $areas = [];

    }

    $this->loadViewsFrom(__DIR__ . '/Views', 'enteweb');

    $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');

    $this->loadMigrationsFrom(__DIR__ . '/Migrations');

    foreach (scandir($path_to_plugins) as $area) {

      if($area != '.' && $area != '..') {

        $this->loadViewsFrom($path_to_plugins . $area . '/Views', strtolower($area));

        $this->loadMigrationsFrom($path_to_plugins . $area . '/Migrations');
      }
    }
  }

  /**
  * Register the application services.
  *
  * @return void
  */
  public function register() {
    //
  }
}
