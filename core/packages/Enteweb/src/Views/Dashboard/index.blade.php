@extends('admin_theme::layouts.app')

@push('styles')
  <link href="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
@endpush

@section('page_title')
  Dashboard
@endsection

@section('titlebar')
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">Dashboard</h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          @lang('enteweb.breadcrumb_here')
          <li>Dashboard</li>
        </ol>
      </div>
    </div>
  @endsection

  @section('content')

    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="white-box">
          <div class="row row-in">
            <div class="col-lg-3 col-sm-6 row-in-br">
              <div class="col-in row">
                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-files-o"></i>
                  <h5 class="text-muted vb">@lang('enteweb.total_pages')</h5> </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3 class="counter text-right m-t-15 text-danger">{{ $pagesCount }}</h3> </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="progress">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                  <div class="col-in row">
                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-newspaper-o"></i>
                      <h5 class="text-muted vb">@lang('enteweb.total_news')</h5> </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <h3 class="counter text-right m-t-15 text-megna">{{ $newsCount }}</h3> </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="progress">
                            <div class="progress-bar progress-bar-megna" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 row-in-br">
                      <div class="col-in row">
                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-picture-o"></i>
                          <h5 class="text-muted vb">@lang('enteweb.total_photo')</h5> </div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <h3 class="counter text-right m-t-15 text-primary">{{ $photosCount }}</h3> </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="progress">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-3 col-sm-6  b-0">
                          <div class="col-in row">
                            <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fa fa-envelope-o"></i>
                              <h5 class="text-muted vb">@lang('enteweb.total_mail')</h5> </div>
                              <div class="col-md-6 col-sm-6 col-xs-6">
                                <h3 class="counter text-right m-t-15 text-success">0</h3> </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--row -->

                    <!-- /.row -->
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                          <h3 class="box-title">@lang('enteweb.total_visited')</h3>
                          <ul class="list-inline text-right">
                            <li>
                              <h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>PC</h5> </li>
                              <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #fb9678;"></i>Mobile</h5> </li>

                                <div id="morris-area-chart" style="height: 340px;"></div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-12 col-lg-12">
                              <div class="white-box">
                                <h3 class="box-title">@lang('enteweb.new_mail')</h3>
                                <div class="message-center">
                                  <a href="#">
                                    <div class="mail-contnet">
                                      <h5>@lang('enteweb.no_email_message')</h5> <span class="mail-desc"></span> <span class="time"></span>
                                    </div>
                                  </a>
                                  {{-- <a href="#">
                                  <div class="user-img"> <img src="../plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                  <div class="mail-contnet">
                                  <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                </a> --}}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- /.row -->


                    @endsection

@push('scripts')
  <script src="{{ asset('/themes/Backoffice/src/assets/plugins/bower_components/morrisjs/morris.js') }}"></script>
  <script src="{{ asset('/themes/Backoffice/src/assets/js/dashboard1.js') }}"></script>
@endpush
