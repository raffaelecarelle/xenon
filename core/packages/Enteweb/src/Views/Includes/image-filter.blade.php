<script type="text/javascript">

$(document).ready(function(){
  var $container = $('#gallery-content-center');
  var button_class = "gallery-header-center-right-links-current";

  $container.isotope({itemSelector : 'img'});

  $("#filter-all").addClass(button_class);

  $("#filter-all").click(function() {
    $container.isotope({ filter: '.all' });
    $('.gallery-header-center-right-links').removeClass(button_class);
    $("#filter-all").addClass(button_class);
  });

  @foreach ($categories as $category)
    $("#filter-{{ $category->slug }}").click(function() {
      $container.isotope({
        filter: '.{{ $category->slug }}'
      });
      $('.gallery-header-center-right-links').removeClass(button_class);
      $("#filter-{{ $category->slug }}").addClass(button_class);
    });
  @endforeach
});

</script>
