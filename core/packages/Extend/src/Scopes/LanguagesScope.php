<?php

namespace Extend\Scopes;

use App;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
class LanguagesScope implements Scope
{
  /**
  * Apply the scope to a given Eloquent query builder.
  *
  * @param  \Illuminate\Database\Eloquent\Builder  $builder
  * @param  \Illuminate\Database\Eloquent\Model  $model
  * @return void
  */
  public function apply(Builder $builder, Model $model)
  {
    return $builder->whereHas('languages', function ($query) {
      $query->where('code', App::getLocale());
    });
  }
}
