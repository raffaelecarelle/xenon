<?php

$includes = [
    /*
    |--------------------------------------------------------------------------
    | Enteweb Includes, please do not remove any lines as it may cause problems
    |--------------------------------------------------------------------------
    */
    'enteweb_header'    =>  "
        <link rel='stylesheet' type='text/css' href='" . asset(Enteweb::publicPath() . '/css/semantic.min.css') . "'>
        <link rel='stylesheet' type='text/css' href='" . asset(Enteweb::publicPath() . '/css/style.css') . "'>
        
        <script src='" . asset('/bower_components/jquery/dist/jquery.min.js') . "'></script>
    ",

    'enteweb_bottom'    =>  "
        <script src='" . asset(Enteweb::publicPath() . '/js/semantic.min.js') . "'></script>
        <script src='" . asset(Enteweb::publicPath() . '/js/script.js') . "'></script>
    ",
];
