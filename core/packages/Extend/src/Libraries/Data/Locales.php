<?php

$locales = [
    'it'    =>  [
        'name'  =>  "Italiano",
        'type'  =>  'flag',
        'type_data' => 'it',
        'enabled'   =>  true
    ],
    'en'    =>  [
        'name'  =>  "English",
        'type'  =>  'flag',
        'type_data' => 'gb',
        'enabled'   =>  true
    ]
];
