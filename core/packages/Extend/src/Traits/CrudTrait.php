<?php

namespace Extend\Traits;

/*
* Laravel classes
*/
use Auth;
use Illuminate\Http\Request;
use ReflectionClass;
use Route;
use View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Session;
use Validator;
use Illuminate\Support\Facades\Schema;

/*
* External libraries
*/
use Carbon\Carbon;

/*
* Ours Libraries
*/
use Extend\Libraries\SweetAlert;


trait CrudTrait {

  public function index(Request $request) {
    $params = Route::current()->parameters();
    $model = $this->getModel();
    $posts = $model;
    $className = get_class($posts);
    $baseNameModel = class_basename($className);
    $posts = $posts->type(strtolower($baseNameModel == 'Post' ? 'Page' : $baseNameModel))->get();
    $routename = Route::current()->getName();
    $view = $this->getView();

    if (View::exists($view)) {
      return view($view)->with('posts', $posts);
    } else {
      return view('admin_theme::error_pages.404');
    }
  }

  public function create() {
    $route_parameters = Route::current()->parameters();
    $model = $this->getModel();
    $row = $model;
    $view = $this->getView();

    if (View::exists($view)) {
      return view($view)->with('row', $row);
    } else {
      return view('admin_theme::error_pages.404');
    }
  }

  public function storeAction($request, $row) {

    $data = $request->all();

    foreach ($data as $k => $v) {
      if (is_array($v)) {
        $v = implode(',', $v);
        $request->merge([$k => $v]);
      }
    }

    $fields = array_keys($row->getAttributes());
    $columns = Schema::getColumnListing($row->getTable());

    if(isset($data['parent_id']) && NULL !== $data['parent_id'] && $data['parent_id'] == "NULL") {
      $data['parent_id'] = NULL;
    }

    $myFields = array_intersect($columns, array_keys($data));

    reset($myFields);
    while (list($k, $v) = each($myFields)) {
      if (isset($this->trad) && array_key_exists($v, $this->trad)) {
        $row->$v = $data[$this->trad[$v]];
      } else {
        $row->$v = $data[$v];
      }
    }
    $row->created_by = Auth::user()->name;
    $row->save();
    return $row;
  }

  public function edit(Request $request) {
    $route_parameters = Route::current()->parameters();
    $id = reset($route_parameters);
    $model = $this->getModel();
    $row = $model->findOrFail($id);
    $view = $this->getView();

    if (View::exists($view)) {
      return view($view)->with('row', $row);
    } else {
      return view('admin_theme::error_pages.404');
    }
  }

  public function updateAction(Request $request, $row) {
    $data = $request->all();
    if(isset($data['parent_id']) && NULL !== $data['parent_id'] && $data['parent_id'] == "NULL") {
      $data['parent_id'] = NULL;
    }

    foreach ($data as $k => $v) {
      if (is_array($v)) {
        $v = implode(',', $v);
        $request->merge([$k => $v]);
      }
    }

    $fields = array_keys($row->getAttributes());
    $myFields = array_intersect($fields, array_keys($data));
    while (list($k, $v) = each($myFields)) {
      if (isset($this->trad) && array_key_exists($v, $this->trad)) {
        $row->$v = $data[$this->trad[$v]];
      } else {
        $row->$v = $data[$v];
      }
    }

    $row->updated_by = Auth::user()->name;
    $row->save();

    return $row;
  }

  public function show(){
    return;
  }

  public function destroy(Request $request) {
    $model = $this->getModel();

    $row = $model->findOrFail($request->id)->delete();

    return redirect()->back();
  }

  public function dupplica() {
    $params = \Route::current()->parameters();
    $model = $this->getModel();
    $id = $params[$this->getPrimaryKey()];
    $row = $model->findOrFail($id)->toArray();
    array_shift($row);
    $duplicateRow = new $model;

    foreach ($row as $k => $v) {
      $duplicateRow[$k] = $v;
    }
    $id_tbl_ = $this->getPrimaryKeyWithLang();
    $duplicateRow->save();
    $duplicateRow->update([$id_tbl_ => $duplicateRow->id]);
    echo "<script>$('#folderTree').jstree(true).refresh(); </script>";
    return SweetAlert::alert('Successo!', 'Risorsa dupplicata correttamente.', 'green');
  }

  public function rename(){
    $params = \Route::current()->parameters();
    $model = $this->getModel();
    $id = $params[$this->getPrimaryKey()];
    $row = $model->findOrFail($id);
    $view = $this->getView();
    $plugin = $this->getViewAlias();
    return view($plugin . $view)->with('params', $params)->with('row', $row);
  }

  public function postRename(Request $request){
    $params = \Route::current()->parameters();
    $model = $this->getModel();
    $id = $params[$this->getPrimaryKey()];
    $row = $model->findOrFail($id);
    $routename = \Route::current()->getName();

    foreach ($request->all() as $k => $v) {
      if (is_array($v)) {
        $v = implode(',', $v);
        $request->merge([$k => $v]);
      }
    }

    $fields = array_keys($row->getAttributes());
    $data = $request->all();
    $myFields = array_intersect($fields, array_keys($data));

    while (list($k, $v) = each($myFields)) {
      if (isset($this->trad) && array_key_exists($v, $this->trad)) {
        $row->$v = $data[$this->trad[$v]];
      } else {
        $row->$v = $data[$v];
      }
    }
    $row->updated_by = Auth::user()->handle;
    $row->updated_at = Carbon::now();
    $row->save();
    return SweetAlert::alert('Successo!', 'Risorsa rinominata correttamente.', 'green');
  }

  public static function getView() {
    $current_route = Route::current()->getName();
    $current_route = explode('.', $current_route);
    array_shift($current_route);
    $alias = array_shift($current_route);
    $view = implode('.', $current_route);
    return $alias . '::' . $view;
  }

  public function getModel(){
    $rc = new ReflectionClass(get_class($this));
    $namespace=$rc->getNamespaceName();
    $str='Controllers';
    $pos=strpos($namespace,$str);
    $namespace=substr($namespace,0,$pos);
    $namespace.='Models\\';
    $class = class_basename(get_class());
    $class = str_replace('Controller', '', $class);
    $model=$namespace.$class;

    return new $model();
  }

  public static function getPrimaryKey(){
    $class = class_basename(get_class());
    $class = str_replace('Controller', '', $class);
    $class=strtolower($class);
    return $class . '_id';
  }
}
