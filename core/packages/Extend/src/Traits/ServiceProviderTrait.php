<?php

namespace XRA\Extend\Traits;
use Illuminate\Foundation\AliasLoader;
use Packages\Packages;
trait ServiceProviderTrait {
  /**
  * Bootstrap the application services.
  *
  * @return void
  */
  public function boot() {
    // questo dir funziona solo per i packages messi in quel punto, devo riuscire a trovare una
    // funzione che mi dia il percorso del package..
    // $dir=dirname(base_path('packages\\'.get_class()).'.php').'\\src';

    $rc = new \ReflectionClass(get_class());
    $dir = dirname($rc->getFileName());

    $class = class_basename(get_class());
    $class = str_replace('ServiceProvider', '', $class);

    $this->loadViewsFrom($dir . '/views', $class);
    $class = strtolower($class);

    // doppio namespace per gestire le view anche in maiuscolo
    // (da mettere solo in minuscolo come da standard)
    $this->loadViewsFrom($dir . '/views', $class);
    $this->loadRoutesFrom($dir . '/routes/web.php');
    $this->loadMigrationsFrom($dir . '/migrations');
    $this->loadTranslationsFrom($dir . '/translations', $class);

    AliasLoader::getInstance()->alias($class,get_class());

    $adm_theme=config('xra.adm_theme');
    $pub_theme=config('xra.pub_theme');

    $this->app['view']->addNamespace('adm_theme', public_path('/themes/'.$adm_theme));
    $this->app['view']->addNamespace('pub_theme', public_path('/themes/'.$pub_theme));
  }


  public function routes() {

    if (\Request::path() != '') {
      $piece=3;
      while($piece>0){

        $tmp = explode('/', \Request::path());
        $tmp = array_slice($tmp, 0, $piece);
        $tmp = implode('_', $tmp);

        $pos=strpos($tmp,':');

        if($pos){
          $tmp=substr($tmp,0,$pos);
        }

        $filename = 'web_' . $tmp . '.php';
        $rc = new \ReflectionClass(get_class($this));
        $dir = dirname($rc->getFileName());
        $tmp = [$dir, 'routes', $filename];
        $filename_dir = implode(DIRECTORY_SEPARATOR, $tmp);

        if (file_exists($filename_dir)) {
          require $filename_dir;
        }

        $piece--;

      }

    }

  }

  public function getNamespace(){

    $rc = new \ReflectionClass(get_class($this));

    return $rc->getNamespaceName();

  }
}
