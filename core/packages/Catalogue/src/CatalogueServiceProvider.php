<?php

namespace Catalogue;

use Illuminate\Support\ServiceProvider;

class CatalogueServiceProvider extends ServiceProvider
{
  /**
  * Bootstrap the application services.
  *
  * @return void
  */
  public function boot()
  {
    $this->loadViewsFrom(__DIR__ . '/Views', 'catalogue');

    $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');

    $this->loadMigrationsFrom(__DIR__ . '/Migrations');

    $this->loadTranslationsFrom(__DIR__.'/Translations', 'catalogue');
  }

  /**
  * Register the application services.
  *
  * @return void
  */
  public function register()
  {
    $this->app->register(Catalogue\CatalogueServiceProvider::class);
  }
}
