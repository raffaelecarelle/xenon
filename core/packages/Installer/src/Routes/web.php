<?php

/*
+---------------------------------------------------------------------------+
| Route di installazione
+---------------------------------------------------------------------------+
*/

Route::group(['middleware' => ['web','enteweb.base'], 'prefix' => 'backoffice', 'namespace' => '\Installer\Controller', 'as' => 'enteweb.'], function () {
	Route::get('/install', function(){
		return redirect()->route('enteweb.install', 'it');
	});
	Route::get('/install/{locale}', 'InstallerController@show')->name('install');
	Route::post('/install/{locale}', 'InstallerController@installConfig');
	Route::get('/install/{locale}/confirm', 'InstallerController@install')->name('install_confirm');

});
