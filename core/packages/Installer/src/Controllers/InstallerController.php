<?php

namespace Installer\Controller;

/*
* Laravel classes
*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Enteweb;
use App;
use Artisan;
use Auth;
use Config;
use Session;
use PDO;
use PDOException;

/*
* Models
*/
use LiveUsers\Models\User;
use LiveUsers\Models\Group;
use LiveUsers\Models\Permission;
use LiveUsers\Models\Application;
use LiveUsers\Models\Area;
use Enteweb\Plugins\Language\Models\Language;
use Enteweb\Plugins\Setting\Models\Setting;

class InstallerController extends Controller
{
  public function show($locale) {
    // Show the installation form
    if(!Enteweb::checkInstalled()){
      // Set the locale
      App::setLocale($locale);

      return view('installer::index');
    } else {
      Session::flash('title', 'Attenzione!');
      Session::flash('message', trans('enteweb.already_installed'));
      Session::flash('type', 'warning');
      return redirect()->route('enteweb.dashboard');
    }
  }

  public function installConfig($locale, Request $request) {

    if(!Enteweb::checkInstalled()) {
      // Install Enteweb
      $this->validate($request, [
        'USER_NAME' => 'required',
        'USER_PASSWORD' => 'required|min:6|confirmed',
        'USER_EMAIL' => 'required',
        'DB_HOST' => 'required',
        'DB_PORT' => 'required',
        'DB_DATABASE' => 'required',
        'DB_USERNAME' => 'required',
        'SUPER_ADMIN_NAME' => 'required'
      ]);

      if(! $this->checkConnectionDatabase($request->all())) {

        return redirect()->route('enteweb.install',$locale)->withInput()
        ->withErrors(['DB_USERNAME' => 'La connessione al database è fallita, controllare i parametri e riprovare.']);

      }

      $file_location = base_path() . '/.env';
      $env = fopen($file_location, "w") or die("Impossibile aprire il file!");

      foreach($request->all() as $key => $data) {
        if($key != '_token' and $key != 'USER_PASSWORD_confirmation') {
          fwrite($env, $key . "='" . $data . "'\n");
        }
      }

      $default = "\nREDIS_HOST=127.0.0.1\nREDIS_PASSWORD=null\nREDIS_PORT=6379\n\nPUSHER_KEY=\nPUSHER_SECRET=\nPUSHER_APP_ID=\n\nBROADCAST_DRIVER=log\nCACHE_DRIVER=file\nSESSION_DRIVER=file\nQUEUE_DRIVER=sync\n\nAPP_ENV=local\nAPP_KEY=" . env('APP_KEY') . "\nAPP_DEBUG=true\nAPP_LOG_LEVEL=debug\nAPP_URL=" . url('/') . "\n";
      fwrite($env, $default);
      fclose($env);

      return redirect()->route('enteweb.install_confirm', ['locale' => $locale]);

    } else {

      Session::flash('title', 'Attenzione!');
      Session::flash('message', trans('enteweb.already_installed'));
      Session::flash('type', 'warning');

      return redirect()->route('enteweb.dashboard');
    }
  }

  public function install(Request $request, $locale) {

    if(!Enteweb::checkInstalled()){

      $exitCode = Artisan::call('migrate');

      $exitCode = Artisan::call('db:seed');

      if (Auth::attempt(['email' => env('USER_EMAIL'), 'password' => env('USER_PASSWORD')])) {
        // Authentication passed...
        $file_location = base_path() . '/.env';

        $default = "\nENTEWEB_INSTALLED=true";

        file_put_contents($file_location,$default, FILE_APPEND);

        $url = route('enteweb.dashboard');

        Session::flash('title', 'Successo!');
        Session::flash('message', trans('enteweb.install_success'));
        Session::flash('type', 'success');

        return redirect()->intended($url);

      } else{

        die("<b>ERRORE: </b> Qualcosa é andato storto, riprova.");

      }

    } else{

      Session::flash('title', 'Attenzione!');
      Session::flash('message', trans('enteweb.already_installed'));
      Session::flash('type', 'warning');

      return redirect()->route('enteweb.dashboard');
    }

  }

  /*
  * Validazione connessione al database
  */
  public function checkConnectionDatabase($request) {

    $db_host = $request['DB_HOST'];

    $db_name = $request['DB_DATABASE'];

    $db_user = $request['DB_USERNAME'];

    $db_password = $request['DB_PASSWORD'];

    try {

      $conn = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return true;

    }
    catch(PDOException $e) {

      return false;

    }
  }
}
