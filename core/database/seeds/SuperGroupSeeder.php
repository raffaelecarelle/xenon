<?php

use Illuminate\Database\Seeder;

use LiveUsers\Models\User;

use LiveUsers\Models\Permission;

use LiveUsers\Models\Group;

class SuperGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      $user = User::first();

      $superAdminPermission = Permission::type(5)->first();

      DB::table('groups')->insert([
        'title' => env('SUPER_ADMIN_NAME'),
        'is_active' => 1,
        'owner_user_id' => $user->id,
        'created_by' => env('SUPER_ADMIN_NAME')
      ]);

      $group = Group::where('title', env('SUPER_ADMIN_NAME'))->first();

      $user->groups()->attach($group->id);

      $group->permissions()->attach($superAdminPermission->id);

    }
}
