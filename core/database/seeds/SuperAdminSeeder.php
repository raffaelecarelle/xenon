<?php

use Illuminate\Database\Seeder;

use LiveUsers\Models\Permission;

use LiveUsers\Models\User;

class SuperAdminSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      
      $user_id = DB::table('users')->insertGetId([
        'name' => env('USER_NAME', 'Q-Web'),
        'username' => env('USER_NAME', 'Q-Web'),
        'email' => env('USER_EMAIL', 'info@qweb.eu'),
        'password' => bcrypt(env('USER_PASSWORD', 'ew8!eb7')),
        'created_by' => env('SUPER_ADMIN_NAME', 'Q-Web')
      ]);

      $superAdminPermission = Permission::type(5)->first();

      $user = User::findOrFail($user_id);

      $user->permissions()->attach($superAdminPermission->id);
    }
}
