<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use LiveUsers\Models\User;
use Enteweb\Plugins\News\Models\NewsSetting;
use Enteweb\Plugins\Language\Models\Language;

class NewsSettingSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $firstUser = User::first();

    $setting_id = DB::table('news_settings')->insertGetId([
      'title' => 'Impostazioni',
      'active' => 1,
      'n_visible_preview' => 8,
      'n_item_x_page_active' => 10,
      'n_item_x_page_archive' => 20,
      'n_item_x_page_search' => 20,
      'max_n_page' => 20,
      'template_list_preview' => 'news_preview',
      'template_list' => 'news_paginate',
      'template' => 'news',
      'template_search' => 'news_search',
      'format_date_list' => 'dd-mm-yyyy',
      'format_date_page' => 'dd-mm-yyyy',
      'format_date_preview' => 'dd-mm-yyyy',
      'locale' => 'it',
      'preview_page_list_active' => 'Da definire',
      'preview_page_view_active' => 'Da definire',
      'preview_page_list_archive' => 'Da definire',
      'preview_page_view_archive' => 'Da definire',
      'preview_page_search' => 'Da definire',
      'preview_page_search_list' => 'Da definire',
      'preview_page_search_view' => 'Da definire',
      'created_by' => $firstUser->name,
      'created_at' => Carbon::now()
    ]);

    $setting = NewsSetting::withoutGlobalScopes()->findOrFail($setting_id);
    $language = Language::currentLanguage('it')->first();
    $setting->languages()->attach($language->id);
  }

}
