<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('permissions')->insert([
      'title' => env('SUPER_ADMIN_NAME'),
      'type' => '5',
      'created_by' => env('SUPER_ADMIN_NAME'),
    ]);

    DB::table('permissions')->insert([
      'title' => 'Amministratore',
      'type' => '4',
      'created_by' => env('SUPER_ADMIN_NAME'),
    ]);

    DB::table('permissions')->insert([
      'title' => 'Supervisore',
      'type' => '3',
      'created_by' => env('SUPER_ADMIN_NAME'),
    ]);

    DB::table('permissions')->insert([
      'title' => 'Segretario',
      'type' => '2',
      'created_by' => env('SUPER_ADMIN_NAME'),
    ]);

    DB::table('permissions')->insert([
      'title' => 'Utente base',
      'type' => '1',
      'created_by' => env('SUPER_ADMIN_NAME'),
    ]);
  }
}
