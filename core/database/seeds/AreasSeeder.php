<?php

use Illuminate\Database\Seeder;

use LiveUsers\Models\Area;
use LiveUsers\Models\Application;
use LiveUsers\Models\Permission;

class AreasSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run() {

    $application = Application::Enteweb()->first();

    DB::table('areas')->insert([
      'title' => 'Pagine',
      'enable' => 1,
      'icon' => 'ti-files',
      'route' => 'enteweb.post.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Notizie',
      'enable' => 1,
      'icon' => 'fa fa-newspaper-o',
      'route' => 'enteweb.news.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    $areaNews = Area::where('title', 'Notizie')->first();

    DB::table('areas')->insert([
      'title' => 'Notizie Attive',
      'enable' => 1,
      'parent_id' => $areaNews->id,
      'icon' => 'fa fa-fire',
      'route' => 'enteweb.news.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Archivio',
      'enable' => 1,
      'parent_id' => $areaNews->id,
      'icon' => 'fa fa-archive',
      'route' => 'enteweb.news.archive.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Impostazioni',
      'enable' => 1,
      'parent_id' => $areaNews->id,
      'icon' => 'fa fa-wrench',
      'route' => 'enteweb.news.setting.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Gallery',
      'enable' => 1,
      'icon' => 'ti-camera',
      'route' => 'enteweb.photo.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Link',
      'enable' => 1,
      'icon' => 'ti-link',
      'route' => 'enteweb.link.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Documenti',
      'enable' => 1,
      'icon' => 'fa fa-clipboard',
      'route' => 'enteweb.document.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);
    DB::table('areas')->insert([
      'title' => 'Modelli',
      'enable' => 1,
      'icon' => 'ti-view-grid',
      'route' => 'enteweb.pattern.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Mail',
      'enable' => 1,
      'icon' => 'ti-email',
      'route' => 'enteweb.mail.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Form',
      'enable' => 1,
      'icon' => 'ti-menu-alt',
      'route' => 'enteweb.form.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Categorie',
      'enable' => 1,
      'icon' => 'ti-tag',
      'route' => 'enteweb.category.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Lingua',
      'enable' => 1,
      'icon' => 'fa fa-flag',
      'route' => 'enteweb.language.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Settings',
      'enable' => 1,
      'icon' => 'fa fa-cogs',
      'route' => 'enteweb.setting.index',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    $areaSetting = Area::where('title', 'Settings')->first();

    DB::table('areas')->insert([
      'title' => 'Applicazione',
      'enable' => 1,
      'parent_id' => $areaSetting->id,
      'route' => 'enteweb.setting.index',
      'icon' => 'fa fa-slack',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Aree',
      'enable' => 1,
      'parent_id' => $areaSetting->id,
      'route' => 'liveusers.area.index',
      'icon' => 'fa fa-sitemap',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Utenti',
      'enable' => 1,
      'parent_id' => $areaSetting->id,
      'route' => 'liveusers.user.index',
      'icon' => 'fa fa-street-view',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Gruppi',
      'enable' => 1,
      'parent_id' => $areaSetting->id,
      'route' => 'liveusers.group.index',
      'icon' => 'fa fa-users',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Permessi',
      'enable' => 1,
      'parent_id' => $areaSetting->id,
      'route' => 'liveusers.permission.index',
      'icon' => 'fa fa-lock',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    DB::table('areas')->insert([
      'title' => 'Diritti',
      'enable' => 0,
      'parent_id' => $areaSetting->id,
      'route' => 'liveusers.right.index',
      'icon' => 'fa fa-hand-paper-o',
      'created_by' => env('SUPER_ADMIN_NAME'),
      'application_id' => $application->id
    ]);

    $superAdminPermission = Permission::type(5)->first();

    foreach (Area::all() as $area) {

      $area->permissions()->attach($superAdminPermission->id);

    }
  }
}
