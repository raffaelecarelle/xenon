<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      DB::table('languages')->insert([
        'title' => 'Italiano',
        'code' => 'it',
        'created_by' => env('SUPER_ADMIN_NAME')
      ]);

      DB::table('languages')->insert([
        'title' => 'Inglese',
        'code' => 'gb',
        'created_by' => env('SUPER_ADMIN_NAME')
      ]);
    }
}
