<?php

use Illuminate\Database\Seeder;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('applications')->insert([
        'title' => env('APP_NAME'),
        'created_by' => env('SUPER_ADMIN_NAME')
      ]);
    }
}
