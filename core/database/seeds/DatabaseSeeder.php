<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      $this->call(ApplicationSeeder::class);

      $this->call(PermissionSeeder::class);

      $this->call(LanguageSeeder::class);

      $this->call(SuperAdminSeeder::class);

      $this->call(NewsSettingSeeder::class);

      $this->call(EntewebSettingSeeder::class);

      $this->call(SuperGroupSeeder::class);

      $this->call(AreasSeeder::class);
    }
}
