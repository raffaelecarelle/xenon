<?php

use Illuminate\Database\Seeder;

use LiveUsers\Models\Application;
use Enteweb\Plugins\Setting\Models\Setting;
use Enteweb\Plugins\Language\Models\Language;

class EntewebSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      $application = Application::Enteweb()->first();

      $setting_id = DB::table('settings')->insertGetId([
        'setting_title' => 'Impostazioni',
        'site_title' => env('APP_NAME'),
        'site_address' => env('APP_URL'),
        'date_format' => env('DATE_FORMAT', 'dd-mm-yyyy'),
        'timezone' => env('TIMEZOME', config('app.timezone')),
        'new_user_default_role' => 1,
        'application_id' => $application->id,
        'active' => 1,
        'created_by' => env('SUPER_ADMIN_NAME')
      ]);
      $setting = Setting::withoutGlobalScopes()->findOrFail($setting_id);
      $language = Language::currentLanguage('it')->first();
      $setting->languages()->attach($language->id);

    }
}
