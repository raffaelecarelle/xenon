<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Install Lines
  |--------------------------------------------------------------------------
  */

  'installer' => "Installazione Enteweb",
  'install_welcome' => 'Benvenuto!',
  'install_app_info'  => "Informazione applicazione",
  'install_app_name' => 'Nome dell\'applicazione',

  'install_personal_info'    => "Informazioni account",
  'install_your_name' => 'Nome',
  'install_your_email' => 'Email',
  'install_your_password' => 'Password',
  'install_your_password_r' => 'Conferma password',

  'install_roles_info'    => "Informazioni Super Admin",
  'install_default_admin_role_name' => 'Nome Super Admin',
  'install_default_role_name' => 'Nome utente base',

  'install_database_info' => "Informationi sul database",
  'install_database_host' => 'Host database',
  'install_database_port' => 'Porta database',
  'install_database_name' => 'Nome Database',
  'install_database_username' => 'Username database',
  'install_database_password' => 'Password database',

  'install_mail_info' => "Informazioni sull'email",
  'install_mail_driver' => 'Driver mail',
  'install_mail_host' => 'Host mail',
  'install_mail_port' => 'Porta mail',
  'install_mail_username' => 'Username mail',
  'install_mail_password' => 'Password mail',
  'install_mail_encryption' => 'Cifratura mail',
  'install_mail_from' => 'From mail',
  'install_mail_name' => 'From name',

  'install_enteweb' => 'Installa Enteweb',
  'already_installed' => 'Enteweb è già stato installato',
  'installing'    => "Installazione di Enteweb in corso...",
  'install_success' => "Enteweb é stato installato con successo!",

  /*
  |--------------------------------------------------------------------------
  | Login Lines
  |--------------------------------------------------------------------------
  */

  'login_welcome' => "Benvenuto nel nuovo Enteweb!",
  'login_remember_me' => 'Ricordami',
  'login_forgot_password'  => "Password dimenticata?",
  'login_in' => 'Login',

  'login_recover_password'    => "Recupera Password",
  'enter_your_email_to_recover_password' => 'Inserisci la tua email per recuperare la password',
  'login_reset' => 'Reset Password',
  'back_to_login' => 'Torna al Login',

  'account_not_active' => 'Il tuo account non è attivo. Contatta l\'amministrazione a: info@qweb.eu',

  /*
  |--------------------------------------------------------------------------
  | Breadcrumbs Lines
  |--------------------------------------------------------------------------
  */

  'breadcrumb_here' => 'Sei qui: ',

  /*
  |--------------------------------------------------------------------------
  | Search Lines
  |--------------------------------------------------------------------------
  */

  'search_placeholder' => 'Cerca...',

  /*
  |--------------------------------------------------------------------------
  | Error page Lines
  |--------------------------------------------------------------------------
  */

  'error_404' => 'Pagina non trovata!',
  'back_to_home' => 'Torna in dashboard',

  /*
  |--------------------------------------------------------------------------
  | Dashboard Lines
  |--------------------------------------------------------------------------
  */

  'total_pages' => 'Pagine',
  'total_visited' => 'Visite totali',
  'total_mail' => 'E-Mail',
  'total_photo' => 'Foto',
  'total_news' => 'News',
  'new_mail' => 'Nuove e-mail ricevute',
  'chat' => 'Chat',
  'send' => 'Invia',
  'no_email_message' => 'Nessuna e-mail ricevuta di recente.',

  /*
  |--------------------------------------------------------------------------
  | Posts Lines
  |--------------------------------------------------------------------------
  */
  'add_keywords' => 'Aggiungi keywords',
  'actions' => 'Azioni',
  'classification' => 'Classificazione',
  'content' => 'Contenuto',
  'delete' => 'Cancella',
  'edit' => 'Modifica',
  'groups' => 'Gruppi',
  'id' => 'ID',
  'info_page' => 'Info pagina',
  'new_page' => 'Crea pagina',
  'meta_tag_author' => 'Meta tag author',
  'meta_tag_description' => 'Meta tag description',
  'meta_tag_title' => 'Meta tag title',
  'parent_id' => 'Padre',
  'order' => 'Posizione',
  'seo' => 'SEO',
  'slug' => 'Rewrite URL',
  'subtitle' => 'Sottotitolo',
  'title' => 'Titolo',
  'user_groups' => 'Gruppi utente',
  'view' => 'Template associato',
  'visible' => 'Stato',
  'category[]' => 'Categoria',
  'select_all' => 'Seleziona tutto',
  'deselect_all' => 'Deseleziona tutto',
  'group_access_resource' => 'Associa i gruppi alla pagina, i quali potranno modificarla/cancellarla.',
  'created_by' => 'Creato da',
  'created_at' => 'Creato in data',
  'updated_by' => 'Modificato da',
  'updated_at' => 'Modificato in data',
  'link[]' => 'Link',
  'edit_page' => 'Modifica pagina',
  'pages' => 'Pagine',
  'info' => 'Informazioni',

  /*
  |--------------------------------------------------------------------------
  | Category Lines
  |--------------------------------------------------------------------------
  */
  'new_category' => 'Crea categoria',
  'categories' => 'Categorie',
  'edit_category' => 'Modifica categoria',

  /*
  |--------------------------------------------------------------------------
  | Language Lines
  |--------------------------------------------------------------------------
  */
  'new_language' => 'Crea lingua',
  'languages' => 'Lingue',
  'edit_language' => 'Modifica lingua',
  'flag' => 'Bandiera',
  'code' => 'Codice',

  /*
  |--------------------------------------------------------------------------
  | Datatables Lines
  |--------------------------------------------------------------------------
  */
  'display_menu_record_per_page' => 'Mostra _MENU_ record per pagina',
  'nothing_found' => 'Ho trovato niente',
  'showing_page_of_pages' => 'Mostra pagina _PAGE_ di _PAGES_',
  'no_record_available' => 'Nessun record disponibile',
  'filtered_from_max_total_records' => '(Ho filtrato da _MAX_ record totali)',
  'first' => 'Prima pagina',
  'last' => 'Ultima pagina',
  'next' => 'Pagina successiva',
  'previous' => 'Pagina precedente',

  /*
  |--------------------------------------------------------------------------
  | SweetAlert Lines
  |--------------------------------------------------------------------------
  */
 'saved' => 'Salvato!',
  'resource_create_success' => 'Risorsa creata con successo!',
  'resource_edit_success' => 'Risorsa modificata con successo!',

  /*
  |--------------------------------------------------------------------------
  | photo Lines
  |--------------------------------------------------------------------------
  */
  'new_album' => 'Crea album',
  'new_photo' => 'Inserisci foto',
  'all' => 'Tutti',
  'photo' => 'Gallery',
  'dropzone' => 'Carica immagini',
  'width' => 'Larghezza',
  'height' => 'Altezza',
  'image_cover' => 'Immagine di copertina',
  'img_alt' => 'Descrizione immagine',
  'img_title' => 'Titolo immagine',
  'edit_album' => 'Modifica album',
  'remove' => 'Rimuovi',
  'drag_and_drop_to_insert' => 'Trascina qui per inserire l\'immagine',
  'drag_and_drop_to_replace' => 'Trascina qui per rimpiazzare l\'immagine',
  'error' => 'ERRORE! Qualcosa è andato storto',
  'choose_image' => 'Scegli immagine',
  'cover_image' => 'Immagine di copertina',
  'image' => 'Immagine',
  /*
  |--------------------------------------------------------------------------
  | Area Lines
  |--------------------------------------------------------------------------
  */
  'new_area' => 'Crea area',
  'route' => 'Rotta d\'accesso',
  'areas' => 'Gestione aree',
  'edit_area' => 'Modifica area',
  'icon' => 'Icona',
  'enable' => 'Abilitata',
  'order_has_been_changed' => 'L\'ordine é stato salvato con successo',
  'order_areas' => 'Ordina le aree',
  'save_order' => 'Salva ordine',
  'permission[]' => 'Permessi',

  /*
  |--------------------------------------------------------------------------
  | User Lines
  |--------------------------------------------------------------------------
  */
  'new_user' => 'Crea utente',
  'users' => 'Gestione utenti',
  'edit_user' => 'Modifica utente',
  'name' => 'Nome',
  'username' => 'Username',
  'email' => 'Email',
  'password' => 'Password',
  'password_confirmation' => 'Conferma password',
  'my_profile' => 'Profilo',
  'is_active' => 'Attivo',
  'permission' => 'Grado',

  /*
  |--------------------------------------------------------------------------
  | Group Lines
  |--------------------------------------------------------------------------
  */
  'new_group' => 'Crea gruppo',
  'groups' => 'Gestione gruppi',
  'edit_group' => 'Modifica Gruppo',

  /*
  |--------------------------------------------------------------------------
  | Permission Lines
  |--------------------------------------------------------------------------
  */
  'new_permission' => 'Crea permesso',
  'permissions' => 'Gestione permessi',
  'edit_permission' => 'Modifica permesso',
  'type' => 'Grado',

  /*
  |--------------------------------------------------------------------------
  | Settings Lines
  |--------------------------------------------------------------------------
  */
  'site_title' => 'Nome sito',
  'tagline' => 'Tagline (di cosa parla il sito?)',
  'site_address' => 'Indirizzo sito (URL)',
  'new_user_default_role' => 'Permesso di default per ogni nuovo utente',
  'timezone' => 'Timezone',
  'date_format' => 'Dateformat',
  'time_format' => 'Timeformat',
  'weeks_start_on' => 'La settimana inizia dal',
  'general' => 'Impostazioni Generali',
  'metatag' => 'Gestione Metatag',
  'theme' => 'Gestione tema',
  'edit_settings' => 'Modifica impostazioni',
  'settings' => 'Impostazioni',
  'enter_to_settings' => 'Entra nelle impostazioni',
  'new_setting' => 'Crea impostazione',
  'setting_title' => 'Nome impostazione',
  'active' => 'Attivo',

  /*
  |--------------------------------------------------------------------------
  | News Lines
  |--------------------------------------------------------------------------
  */
  'end_published_at' => 'Data fine pubblicazione',
  'new_news' => 'Crea notizia',
  'news' => 'Gestione notizie',
  'in_home' => 'Pubblicato in Home page',
  'published_at' => 'Data di pubblicazione',
  'end_published_in_home_at' => 'Data fine pubblicazione in home page',
  'end_published_at' => 'Data fine pubblicazione',
  'published_in_home_at' => 'Data pubblicazione in home page',
  'dates' => 'Pubblicazione',
  'news_archive' => 'Archivio',
  'edit_news' => 'Modifica news',

  /*
  |--------------------------------------------------------------------------
  | News Settings Lines
  |--------------------------------------------------------------------------
  */
  'templates' => 'Template',
  'format_dates' => 'Date',
  'pages_for_news_actived' => 'News attive',
  'pages_for_news_archived' => 'News archiviate',
  'pages_for_news_searched' => 'Ricerca',

  'n_visible_preview' => 'Numero di ultime news attive visibili in anteprima',
  'n_item_x_page_archive' => 'Numero di news in archivio visibili per pagina',
  'max_n_page' => 'Numero di pagine visualizzabili nell\'indice di paginazione',
  'n_item_x_page_active' => 'Numero di news attive visibili per pagina',
  'n_item_x_page_search' => 'Numero di news ricercate visibili per pagina',

  'template_list_preview' => 'Template di default per la lista delle news in homepage',
  'template_list' => 'Template di default per la lista delle news',
  'template' => 'Template di default per la visualizzazione di una news',
  'template_search' => 'Template di default per il modulo di ricerca news',

  'format_date_list' => 'Seleziona il formato delle date nell\'elenco delle news',
  'format_date_page' => 'Seleziona il formato delle date nella pagina della news',
  'format_date_preview' => 'Seleziona il formato delle date nella pagina di anteprima delle news',
  'locale' => 'Seleziona la lingua per le date',

  'preview_page_list_active' => 'Pagina che elenca le news attive',
  'preview_page_view_active' => 'Pagina che visualizza una news attiva',

  'preview_page_list_archive' => 'Pagina che elenca le news in archivio',
  'preview_page_view_archive' => 'Pagina che visualiza una news in archivio',

  'preview_page_search' => 'Pagina che visualizza il modulo di ricerca delle news',
  'preview_page_search_list' => 'Pagina che visualizza i risultati della ricerca delle news',
  'preview_page_search_view' => 'Pagina che visualizza una news ricercata',

  /*
  |--------------------------------------------------------------------------
  | Link Lines
  |--------------------------------------------------------------------------
  */
  'new_link' => 'Crea Link',
  'links' => 'Gestione link',
  'edit_link' => 'Modifica link',
  'link' => 'Link',

  /*
  |--------------------------------------------------------------------------
  | Form Lines
  |--------------------------------------------------------------------------
  */
  'new_form' => 'Crea form',
  'forms' => 'Gestione form',
  'edit_form' => 'Modifica form',
  'form' => 'Form',
  'build_your_form' => 'Costruisci il tuo form!',

  /*
  |--------------------------------------------------------------------------
  | Form Lines
  |--------------------------------------------------------------------------
  */
  'new_mail' => 'Scrivi mail',
  'mails' => 'Gestione Email',
  'mail_detail' => 'Dettaglio mail',
  'email' => 'Email',
  'sent_mail' => 'Email inviate',
  'inbox' => 'Email Ricevute',
  'trash' => 'Cestino',
  'filter' => 'Filtro',
  'starred' => 'Evidenziate',
  'read' => 'Lette',
  'unread' => 'Non lette',
  'to' => 'Scrivi a:',
  'mail_to' => 'a:',
  'from' => 'da:',
  'subject' => 'Oggetto:',
  'enter_text' => 'Scrivi...',
  'attachment' => 'Allega file',
  'attachments' => 'Allegati',
  'discard' => 'Resetta',
  'compose_new_message' => 'Componi una nuova mail',
  'email_send_with_success' => 'Email inviata con successo!',

  /*
  |--------------------------------------------------------------------------
  | Form Lines
  |--------------------------------------------------------------------------
  */
  'documents' => 'File Manager'
];
