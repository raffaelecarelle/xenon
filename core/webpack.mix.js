const { mix } = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles(
  [
  '../public_html/themes/Backoffice/src/assets/bootstrap/dist/css/bootstrap.min.css',
  '../public_html/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css',
  '../public_html/themes/Backoffice/src/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
  '../public_html/themes/Backoffice/src/assets/plugins/bower_components/toast-master/css/jquery.toast.css',
  '../public_html/themes/Backoffice/src/assets/plugins/bower_components/morrisjs/morris.css',
  '../public_html/themes/Backoffice/src/assets/css/animate.css',
  '../public_html/themes/Backoffice/src/assets/css/style.css',
  '../public_html/themes/Backoffice/src/assets/css/colors/blue.css',
], '../public_html/css/all.css');

mix.js(
     [
       '../public_html/themes/Backoffice/src/assets/plugins/bower_components/jquery/dist/jquery.min.js',
       '../public_html/themes/Backoffice/src/assets/bootstrap/dist/js/tether.min.js',
       '../public_html/themes/Backoffice/src/assets/bootstrap/dist/js/bootstrap.min.js',
       '../public_html/themes/Backoffice/src/assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js',
       '../public_html/themes/Backoffice/src/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
       '../public_html/themes/Backoffice/src/assets/js/jquery.slimscroll.js',
       '../public_html/themes/Backoffice/src/assets/plugins/bower_components/raphael/raphael-min.js',
       '../public_html/themes/Backoffice/src/assets/plugins/bower_components/morrisjs/morris.js',
       '../public_html/themes/Backoffice/src/assets/js/custom.min.js',
       '../public_html/themes/Backoffice/src/assets/js/dashboard1.js',
       '../public_html/themes/Backoffice/src/assets/plugins/bower_components/toast-master/js/jquery.toast.js',
       '../public_html/themes/Backoffice/src/assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
   ], '../../public_html/js/all.js');
