$(document).ready(function() {
  window.tinymce.dom.Event.domLoaded = true;
  var editor_config = {
    baseURL: base_url,
    path_absolute: base_url,
    selector: "textarea#mymce",
    setup: function (editor) {
      editor.on('change', function () {
        tinymce.triggerSave();
      });
    },
    relative_urls: false,
    themes: "modern",
    schema: 'html5',
    height : "480",
    filemanager_crossdomain: true,
    image_advtab: true ,
    //external_filemanager_path: base_url + "enteweb_public/tinymce/plugins/filemanager/",
    external_plugins: {
        'bootstrap': base_url + 'enteweb_public/tinymce/plugins/bootstrap/plugin.js',
        //'filemanager': base_url + 'enteweb_public/tinymce/plugins/filemanager/plugin.min.js',
        //'responsivefilemanager': base_url + 'enteweb_public/tinymce/plugins/responsivefilemanager/plugin.min.js',
    },
    bootstrapConfig: {
      //TODO: inserire file secure.php (anche vuoto) nella directory per consentire l'accesso
      'imagesPath': base_url + 'enteweb_public/file-manager/files/'
    },
    plugins: [
      'advlist autolink lists image charmap print preview hr anchor pagebreak', 'bootstrap', 'youtube',
      'searchreplace wordcount visualblocks visualchars code fullscreen link',
      'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons template paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: "bootstrap | link unlink anchor | image media | youtube | forecolor backcolor  | print preview code",
    toolbar2: "insertfile | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fullscreen",

    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.baseURL = base_url + '/themes/Backoffice/src/assets/plugins/bower_components/tinymce'; // obbligatorio
  tinymce.init(editor_config);
});
